package DAO;

import Negocio.BookClass;
import Negocio.PrestamoClass;
import Negocio.UserClass;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Abraham Fernandez Sanchez
 */
public class ClaseBBDD {

    Connection conn;
    Statement sentencia;
    ResultSet resul;
    UserClass user;
    PrestamoClass prestamo;

    /**
     * Constructor de clase
     *
     * @throws SQLException Excepcion de tipo SQL
     */
    public ClaseBBDD() throws SQLException {
        dbExists();

    }

//---- Metodos de Acceso a BBDD
    /**
     * Conexion general con BBDD<br>
     *
     * Metodo para la conexion de java con la base de datos MySql
     *
     * @throws SQLException Excepcion de tipo SQL
     */
    public void conexion() throws SQLException {
        // Establecemos la conn con la BD
        //Primer campo "biblioteca" Nombre de la BD, Segundo campo "root" Usuario,Tercer campo Clave "".
        this.conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/biblioteca", "root", "");
    }

    /**
     * Metodo para controlar si esta creada la conexion con mysql o no
     * @return boolean
     */
    public boolean conexionMYSQL(){
        
        try {
            this.conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/sys", "root", "");
            return true;
        } catch (SQLException ex) {
//            Logger.getLogger(ClaseBBDD.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    
    }
    /**
     * Metodo para saber si la base de datos 'notaria' existe
     *
     * @return Boolean
     */
    public boolean dbExists() {
        try {
            String db = "biblioteca";
            String pass = "";
            Class.forName("com.mysql.jdbc.Driver");
            Connection conexion = (Connection) DriverManager.getConnection("jdbc:mysql://localhost/sys", "root", "");
            sentencia = (Statement) conexion.createStatement();
            String sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '" + db + "'";
            ResultSet rs = sentencia.executeQuery(sql);
            if (rs.next()) {
                conexion();
                return true;
            }
            conexion.close();
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("La bbdd no existe");
            return false;
        }
        return false;
    }

    /**
     * Metodo que crea una base datos y devuelve booleano con su respuesta
     *
     * @return Boolean
     */
    public boolean creacionBBDD() {
        String url = "jdbc:mysql://localhost";
        String username = "root";
        String password = "";
        String sql = "CREATE DATABASE IF NOT EXISTS biblioteca";
        try (java.sql.Connection conexion = DriverManager.getConnection(url, username, password);
                java.sql.PreparedStatement stmt = conexion.prepareStatement(sql)) {
            stmt.execute();
            return true;
        } catch (Exception e) {
            System.out.println("error en la creacion " + e);
            return false;
        }
    }

    /**
     * Metodo para crear las tablas para la database biblioteca
     *
     */
    public void creacionTablas() {
        try {
            String url = "jdbc:mysql://localhost/biblioteca";
            String username = "root";
            String password = "";
            java.sql.Connection conexion = DriverManager.getConnection(url, username, password);
            java.sql.PreparedStatement stmt = null;

            stmt = conexion.prepareStatement("CREATE TABLE administrador (id int(11) NOT NULL, nombre varchar(20) NOT NULL, password varchar(20) NOT NULL)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE administrador  ADD PRIMARY KEY (id)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE administrador  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1");
            stmt.execute();

            stmt = conexion.prepareStatement("CREATE TABLE libro (id int(11) NOT NULL, isbn int(20) NOT NULL, titulo varchar(100) NOT NULL, editorial varchar(100) NOT NULL, carnet_usuario int(11) DEFAULT NULL)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE libro  ADD PRIMARY KEY (id) , ADD UNIQUE KEY isbn (isbn)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE libro  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106");
            stmt.execute();

            stmt = conexion.prepareStatement("CREATE TABLE prestamos_libros (id int(11) NOT NULL, isbn int(20) NOT NULL, carnet_usuario int(11) NOT NULL, f_salida date DEFAULT NULL, f_devolucion date DEFAULT NULL)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE prestamos_libros  ADD PRIMARY KEY (id) , ADD UNIQUE KEY id (id)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE prestamos_libros  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18");
            stmt.execute();

            stmt = conexion.prepareStatement("CREATE TABLE usuario (id int(11) NOT NULL, carnet int(11) NOT NULL, nombre varchar(20) NOT NULL, apellidos varchar(50) NOT NULL, direccion varchar(200) NOT NULL,telefono varchar(12) NOT NULL, email varchar(50) NOT NULL, password varchar(20) NOT NULL)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE usuario  ADD PRIMARY KEY (id) , ADD UNIQUE KEY carnet (carnet), ADD UNIQUE KEY email (email)");
            stmt.execute();
            stmt = conexion.prepareStatement("ALTER TABLE usuario  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128");
            stmt.execute();

            //insercion de un administrador tipo, obligatorio para poder acceder a la base de datos
            stmt = conexion.prepareStatement("INSERT INTO administrador (nombre, password) VALUES ('admin', 'admin')");
            stmt.execute();

            stmt.close();

        } catch (SQLException sqle) {
            System.out.println("Error en la ejecución: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());
        }
    }

    public void insercionDatosEjemplo() {
        try {
            String url = "jdbc:mysql://localhost/biblioteca";
            String username = "root";
            String password = "";
            java.sql.Connection conexion = DriverManager.getConnection(url, username, password);
            java.sql.PreparedStatement stmt = null;

            stmt = conexion.prepareStatement("INSERT INTO administrador (nombre, password) VALUES ('abraham', '1111')");
            stmt.execute();
            stmt = conexion.prepareStatement(getRowExampleInsertLibros());
            stmt.execute();
            stmt = conexion.prepareStatement(getRowExampleInsertUsuarios());
            stmt.execute();
            stmt = conexion.prepareStatement(getRowExampleInsertPrestamos());
            stmt.execute();

            stmt.close();

        } catch (SQLException sqle) {
            System.out.println("Error en la ejecución: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());
        }
    }

// END -- Metodos de Acceso a BBDD   
//----------------------------------------------------------------------    
// METODOS PARA LA ADMINISTRADOR    
    /**
     * Actualizacion de Password de Administrador<br>
     *
     * Método que actualiza el password del administrador actualmente logeado
     *
     * @param nomAdmin parametro cuyo valor correspondera al nombre de un
     * administrador
     * @param oldPassword parametro cuyo valor correspondera con el password
     * antiguo
     * @param newPassword parametro cuyo valor correspondera con el nuevo
     * password
     * @throws SQLException Excepcion de tipo SQL
     */
    public void updatePasswordAdministrator(String nomAdmin, String oldPassword, String newPassword) throws SQLException {
        // the mysql insert statement
        String query = "UPDATE administrador SET password=? WHERE nombre=? AND password=? ";
        System.out.println(query);
        // create the mysql insert preparedstatement                
        PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query);
        preparedStmt.setString(1, newPassword);
        preparedStmt.setString(2, nomAdmin);
        preparedStmt.setString(3, oldPassword);
        // execute the preparedstatement
        preparedStmt.execute();
        preparedStmt.close();
    }

    /**
     * Existencia de un Administrador<br>
     *
     * Método que devulve si un administrador con una contraseña concreta existe
     * o no
     *
     * @param nombre parametro cuyo valor correspondera al nombre de un
     * administrador
     * @param password parametro cuyo valor correspondera al password del
     * administrador
     * @return retornara "true" si existe un administrador actualmente logeado,
     * en caso contrario no
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isAdministrator(String nombre, String password) throws SQLException {
        int existeAdmin = 0;//control para la existencia de un admin       
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM administrador WHERE nombre = '" + nombre + "' AND password='" + password + "' ");
        while (resul.next()) {
            //si existe algun admin con los datos requeridos estara en un registro y por lo tanto se sumara uno a la variable       
            existeAdmin = resul.getRow();
        }
        if (existeAdmin == 1) {
            return true;
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return false;
    }

// END METODOS PARA LA ADMINISTRADO    
//----------------------------------------------------------------------    
// METODOS PARA LA TABLA USUARIO    
    /**
     * Obtencion de un usuario de BBDD<br>
     *
     * Metodo con el que se obtendra un objeto con los datos del usuario
     * solicitado
     *
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @return retorna un objeto de la clase UserClass
     * @throws SQLException Escepcion de tipo SQL
     */
    public UserClass getObjectUser(String nCarnet) throws SQLException {
        int numCarnet = Integer.valueOf(nCarnet);
        String nombre = "", apellidos = "", direccion = "", tlf = "", email = "", password = "";
        // Preparamos la consulta
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM usuario WHERE carnet=" + numCarnet);
        while (resul.next()) {
            nombre = resul.getString(3);
            apellidos = resul.getString(4);
            direccion = resul.getString(5);
            tlf = resul.getString(6);
            email = resul.getString(7);
            password = resul.getString(8);
        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return new UserClass(nCarnet, nombre, apellidos, direccion, tlf, email, password);
    }

    /**
     * Añadir de usuario en la BBDD<br>
     *
     * Método que inserta un nuevo usuario en la base de datos
     *
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @param nombre parametro cuyo valor correspondera con el numero de nombre
     * del usuario
     * @param apellidos parametro cuyo valor correspondera con los apellidos del
     * usuario
     * @param direccion parametro cuyo valor correspondera con la direccion del
     * usuario
     * @param tlf parametro cuyo valor correspondera con el numero de telefono
     * del usuario
     * @param email parametro cuyo valor correspondera con el email del usuario
     * @param password parametro cuyo valor correspondera con el password del
     * usuario
     * @throws SQLException Excepcion de tipo SQL
     */
    public void insertUserBBDD(String nCarnet, String nombre, String apellidos, String direccion, String tlf, String email, String password) throws SQLException {
        if (!isUserCarnetNumber(nCarnet) && !isUserEmail(email)) {
            int numCarnet = Integer.valueOf(nCarnet);
            // the mysql insert statement
            String query = " INSERT INTO usuario (carnet,nombre,apellidos,direccion,telefono,email,password)"
                    + " values (?, ?, ?, ?, ?, ?, ?)";
            try ( // create the mysql insert preparedstatement
                    PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
                preparedStmt.setInt(1, numCarnet);
                preparedStmt.setString(2, nombre);
                preparedStmt.setString(3, apellidos);
                preparedStmt.setString(4, direccion);
                preparedStmt.setString(5, tlf);
                preparedStmt.setString(6, email);
                preparedStmt.setString(7, password);
                // execute the preparedstatement
                preparedStmt.execute();
                preparedStmt.close();
            }
        }
    }

    /**
     * Borrar usuario de la BBDD<br>
     *
     * Metodo para el borrado de un registro concreto en la base de datos
     *
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @throws SQLException Excepcion de tipo SQL
     */
    public void deleteUserBBDD(String nCarnet) throws SQLException {
        int numCarnet = Integer.valueOf(nCarnet);
        // the mysql insert statement
        String query = " DELETE FROM usuario WHERE carnet=?";
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setInt(1, numCarnet);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Actualizacion de un usuario de la BBDD<br>
     *
     * Metodo para actualizar los datos de un registro de la base de datos
     *
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @param nombre parametro cuyo valor correspondera con el numero de nombre
     * del usuario
     * @param apellidos parametro cuyo valor correspondera con los apellidos del
     * usuario
     * @param direccion parametro cuyo valor correspondera con la direccion del
     * usuario
     * @param tlf parametro cuyo valor correspondera con el numero de telefono
     * del usuario
     * @param email parametro cuyo valor correspondera con el email del usuario
     * @param password parametro cuyo valor correspondera con el password del
     * usuario
     * @throws SQLException Excepcion de tipo SQL
     */
    public void updateUserBBDD(String nCarnet, String nombre, String apellidos, String direccion, String tlf, String email, String password) throws SQLException {
        int numCarnet = Integer.valueOf(nCarnet);
        // the mysql insert statement
        String query = "UPDATE usuario SET nombre=? ,apellidos=? ,direccion=? ,telefono=? ,email=? ,password=? WHERE carnet=?";
        System.out.println(query);
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setString(1, nombre);
            preparedStmt.setString(2, apellidos);
            preparedStmt.setString(3, direccion);
            preparedStmt.setString(4, tlf);
            preparedStmt.setString(5, email);
            preparedStmt.setString(6, password);
            preparedStmt.setInt(7, numCarnet);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Existencia de un Numero de Carnet de usuario<br>
     *
     * Metodo que devuelve si un Numero de Carnet ya existe en la BBDD o no
     *
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @return retorna "true" o "false" si un numero de carnet de usuario existe
     * o no en la base de datos
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isUserCarnetNumber(String nCarnet) throws SQLException {
        try {
            int numCarnet = Integer.valueOf(nCarnet);
            int existeNcarnet = 0;//control para la existencia de un numero de carnet    
            sentencia = (Statement) conn.createStatement();
            resul = sentencia.executeQuery("SELECT * FROM usuario WHERE carnet='" + numCarnet + "' ");
            while (resul.next()) {
                //si existe algun numero de carnet       
                existeNcarnet = resul.getRow();
                return true;
            }
            if (existeNcarnet == 0) {
                return false;
            }
            resul.close();// Cerrar ResultSet
            sentencia.close();// Cerrar Statement            

        } catch (NumberFormatException ex) {
        }
        return false;
    }

    /**
     * Existencia de un email<br>
     *
     * Metodo que devuelve si un email ya existe en la BBDD o no
     *
     * @param email parametro cuyo valor correspondera con el email del usuario
     * @return retorna "true" o "false" si un email existe o no en la base de
     * datos
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isUserEmail(String email) throws SQLException {
        int emailCount = 0;//control para la existencia de un numero de carnet    
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM usuario WHERE email='" + email + "' ");
        while (resul.next()) {
            //si existe algun numero de carnet       
            emailCount = resul.getRow();
            return true;
        }
        if (emailCount == 0) {
            return false;
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return false;
    }

// END -- METODOS PARA LA TABLA USUARIO        
//----------------------------------------------------------------------    
// METODOS PARA LA TABLA LIBRO    
    /**
     * Obtencion de un libro de la base de datos<br>
     *
     * Metodo con el que se obtendra un objeto con los datos del libro
     * solicitado, correspondiente al numero ISBN introducido como parametro
     *
     * @param numIsbn parametro cuyo valor correspondera con el numero de ISBN
     * de un libro
     * @return retorna un objeto de la clase BookClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public BookClass getObjectBook(String numIsbn) throws SQLException {
        int nisbn = Integer.parseInt(numIsbn);
        String titulo = "", editorial = "", carnetUsu = "";
        // Preparamos la consulta
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM libro WHERE isbn=" + nisbn);
        // Recorremos el resultado para visualizar cada fila
        // Se hace un bucle mientras haya registros, se van visualizando
        while (resul.next()) {
            titulo = resul.getString(3);
            editorial = resul.getString(4);
            carnetUsu = resul.getString(5);
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return new BookClass(numIsbn, titulo, editorial);
    }

    /**
     * Añadir un nuevo libro en BBDD<br>
     *
     * Metodo con el que se introducira un nuevo registro compuesto por los
     * datos de un libro
     *
     * @param isbn parametro cuyo valor correspondera con el numero de ISBN de
     * un libro
     * @param titulo parametro cuyo valor correspondera con el nombre de un
     * libro
     * @param editorial parametro cuyo valor correspondera con la editorial de
     * un libr
     * @throws SQLException Excepcion de tipo SQL
     */
    public void insertBookBBDD(int isbn, String titulo, String editorial) throws SQLException {
        // the mysql insert statement
        String query = " INSERT INTO libro (isbn,titulo,editorial)"
                + " values (?, ?, ?)";
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setInt(1, isbn);
            preparedStmt.setString(2, titulo);
            preparedStmt.setString(3, editorial);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Borrado de un libro en la BBDD<br>
     *
     * Metodo que borrara el registro que contenga un numero ISBN igual que el
     * introducido
     *
     * @param nISBN parametro cuyo valor correspondera con el numero de ISBN de
     * un libro
     * @throws SQLException Excepcion de tipo SQL
     */
    public void deleteBookBBDD(int nISBN) throws SQLException {
        // the mysql insert statement
        String query = " DELETE FROM libro WHERE isbn=?";
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setInt(1, nISBN);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Actualizar datos de un libro<br>
     *
     * Metodo con el que se actualizara el registro correspondiente al libro
     * seleccionado
     *
     * @param isbn parametro cuyo valor correspondera con el numero de ISBN de
     * un libro
     * @param titulo parametro cuyo valor correspondera con el nombre de un
     * libro
     * @param editorial parametro cuyo valor correspondera con la editorial de
     * un libro
     * @throws SQLException Excepcion de tipo SQL
     */
    public void updateBookBBDD(String isbn, String titulo, String editorial) throws SQLException {
        int numIsbn = Integer.parseInt(isbn);
        // the mysql insert statement
        String query = "UPDATE libro SET isbn=? ,titulo=? ,editorial=?  WHERE isbn=?";
        System.out.println(query);
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setInt(1, numIsbn);
            preparedStmt.setString(2, titulo);
            preparedStmt.setString(3, editorial);
            preparedStmt.setInt(4, numIsbn);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Existencia del numero ISBN de un libro<br>
     *
     * Metodo que comprueba la existencia de un libro por medio de su numero
     * ISBN devolviendo true o false
     *
     * @param nISBN parametro cuyo valor correspondera con el numero de ISBN de
     * un libro
     * @return retorna "true" si el numero ISBN existe, en caso contrario
     * retorna "false"
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isISBNExists(String nISBN) throws SQLException {
        int Isbn = Integer.valueOf(nISBN);
        int existeIsbn = 0;//control para la existencia de un numero de carnet    
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM libro WHERE isbn='" + Isbn + "' ");
        while (resul.next()) {
            //si existe algun numero de carnet       
            existeIsbn = resul.getRow();
            return true;
        }
        if (existeIsbn == 0) {
            return false;
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return false;
    }

// END --  METODOS PARA LA TABLA LIBRO 
//----------------------------------------------------------------------    
// METODOS PARA LA TABLA PRESTAMOS_LIBROS  
    /**
     * Creacion de un Prestamo de Libro<br>
     *
     * Metodo que realiza un INSERT TO en la Tabla prestamos_libros de la Base
     * de datos biblioteca con el ojetivo de crear un nuevo registro de prestamo
     * en la tabla
     *
     * @param prestamoClass Objeto de la clase PrestamoClass
     * @throws SQLException Excepcion de tipo SQL
     * @throws ParseException Excepcion de tipo parse
     */
    public void insertPrestamoLibroBBDD(PrestamoClass prestamoClass) throws SQLException, ParseException {
        int isbn = Integer.parseInt(prestamoClass.getIsbn());
        int carnet = Integer.parseInt(prestamoClass.getnCarnet());
        Date fecha = prestamoClass.getFechaSalida();

        String query = " INSERT INTO prestamos_libros (isbn,carnet_usuario,f_salida)"
                + " values (?, ?, ?)";
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setInt(1, isbn);
            preparedStmt.setInt(2, carnet);
            preparedStmt.setDate(3, fecha);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Finalizacion de un prestamo<br>
     *
     * Metodo que realizara un UPDATE actualizando el campo que contiene la
     * fecha de devolucion
     *
     * @param prestamoClass Objeto de la clase PrestamoClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public void finalizarPrestamoLibroBBDD(PrestamoClass prestamoClass) throws SQLException {
        int id = prestamoClass.getId();
        int numIsbn = Integer.parseInt(prestamoClass.getIsbn());
        int nCarnet = Integer.parseInt(prestamoClass.getnCarnet());
        Date fechaSalida = prestamoClass.getFechaSalida();
        Date fechaDevolucion = prestamoClass.getFechaDevolucion();

        // the mysql insert statement
        String query = "UPDATE prestamos_libros SET f_devolucion=? WHERE id=? AND isbn=? AND f_salida=? ";
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setDate(1, fechaDevolucion);
            preparedStmt.setInt(2, id);
            preparedStmt.setInt(3, numIsbn);
            preparedStmt.setDate(4, fechaSalida);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

    /**
     * Obtencion de un Objeto de la clase PrestamoClass<br>
     * Se obtendra un objeto con los parametros suficientes para la busqueda de
     * la referencia a actualizar y en este caso se trata de devolver un libro
     * por lo que se necesita el num id de la fila de la tabla.
     *
     * @param Isbn parametro cuyo valor correspondera con el numero isbn del
     * libro
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @return devolvera un objeto de la clase PrestamoClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public PrestamoClass getObjectPrestamo(String Isbn, String nCarnet) throws SQLException {
        int id = 0;
        int isbn = Integer.valueOf(Isbn);
        int numCarnet = Integer.valueOf(nCarnet);
        Date fechaSalida = null;
        Date fechaDevollucion = null;

        // Preparamos la consulta
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM prestamos_libros WHERE isbn=" + isbn + " AND carnet_usuario=" + numCarnet);
        while (resul.next()) {
            id = resul.getInt(1);
            fechaSalida = resul.getDate(4);
            fechaDevollucion = resul.getDate(5);
        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return new PrestamoClass(id, Isbn, nCarnet, fechaSalida, fechaDevollucion);
    }

    /**
     * Listado de todos los prestamos sin condiciones<br>
     * Metodo que devolvera un arrayList que contendra objetos de los prestamos
     * que contemplen los requisitos solicitados en la query interna, en este
     * caso ninguna
     *
     * @return retornara un arrayList de objetos de la clase PrestamoClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public ArrayList listadoAllPrestamos() throws SQLException {
        int id, isbn, numCarnet;
        Date fechaSalida, fechaDevollucion;

        ArrayList<PrestamoClass> objPrestamosArrayList = new ArrayList<>();

        // Preparamos la consulta
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM prestamos_libros");
        while (resul.next()) {
            id = resul.getInt(1);
            isbn = resul.getInt(2);
            numCarnet = resul.getInt(3);
            fechaSalida = resul.getDate(4);
            fechaDevollucion = resul.getDate(5);

            prestamo = new PrestamoClass(id, String.valueOf(isbn), String.valueOf(numCarnet), fechaSalida, fechaDevollucion);
            mostrarPrestamos(prestamo);
            objPrestamosArrayList.add(prestamo);

        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar         

        return objPrestamosArrayList;
    }

    /**
     * Listado de los prestamos actuales en vigor de un usuario<br>
     * Metodo que devolvera un arrayList que contendra objetos de los prestamos
     * que contemplen los requisitos solicitados en la query interna
     *
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @return retornara un arrayList de objetos de la clase PrestamoClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public ArrayList listadoPrestamosActualesUsuario(String nCarnet) throws SQLException {
        int id, isbn, numCarnet;
        Date fechaSalida, fechaDevollucion;

        ArrayList<PrestamoClass> objPrestamosArrayList = new ArrayList<>();

        // Preparamos la consulta
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM prestamos_libros WHERE carnet_usuario = " + nCarnet + " AND f_devolucion IS NULL");
        while (resul.next()) {
            id = resul.getInt(1);
            isbn = resul.getInt(2);
            numCarnet = resul.getInt(3);
            fechaSalida = resul.getDate(4);
            fechaDevollucion = resul.getDate(5);

            prestamo = new PrestamoClass(id, String.valueOf(isbn), String.valueOf(numCarnet), fechaSalida, fechaDevollucion);
            mostrarPrestamos(prestamo);
            objPrestamosArrayList.add(prestamo);

        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar         

        return objPrestamosArrayList;
    }

    /**
     * Listado del historial de prestamos<br>
     * Metodo que solicita los datos de los prestamos realizados y que no esten
     * en curso actualmente (no estan devueltos)
     *
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet
     * del usuario
     * @return retorna un arrayList que contiene los objetos de la clase
     * prestamos que cumplan las condiciones de la query
     * @throws SQLException Excepcion de tipo SQL
     */
    public ArrayList listadoHistorialPrestamosUsuario(String nCarnet) throws SQLException {
        int id, isbn, numCarnet;
        Date fechaSalida, fechaDevollucion;

        ArrayList<PrestamoClass> objPrestamosArrayList = new ArrayList<>();

        // Preparamos la consulta
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT * FROM prestamos_libros WHERE carnet_usuario = " + nCarnet + " AND f_devolucion IS NOT NULL");
        while (resul.next()) {
            id = resul.getInt(1);
            isbn = resul.getInt(2);
            numCarnet = resul.getInt(3);
            fechaSalida = resul.getDate(4);
            fechaDevollucion = resul.getDate(5);

            prestamo = new PrestamoClass(id, String.valueOf(isbn), String.valueOf(numCarnet), fechaSalida, fechaDevollucion);
            mostrarPrestamos(prestamo);
            objPrestamosArrayList.add(prestamo);

        }//end while
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar         

        return objPrestamosArrayList;
    }

    /**
     * Muestra basica de prestamos<br>
     * Metodo que mostrara por panatalla los datos de un prestamo solicitado
     *
     * @param prestamo objeto de la clase PrestamoClass
     */
    public void mostrarPrestamos(PrestamoClass prestamo) {
//        System.out.println("id: "+prestamo.getId()+" isbn: "+prestamo.getIsbn()+" carnet: "+prestamo.getnCarnet()+" f_salida: "+prestamo.getFechaSalida()+" f_devolucion: "+prestamo.getFechaDevolucion());        

    }

    /**
     * Existencia del prestamo de un libro actualmente<br>
     * Metodo que comprueba la existencia del prestamo de un libro por medio del
     * numero ISBN del libro a consultar.
     *
     * @param prestamoClass Objeto de la clase PrestamoClass
     * @return retorna "true" si algun usuario tiene el libro solicitado en
     * prestamo actualmente, en caso contrario "false".
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isBookPrestado(PrestamoClass prestamoClass) throws SQLException {

//        System.out.println("dentro de isBookPrestado");
        int Isbn = Integer.valueOf(prestamoClass.getIsbn());
        int nCarnet = Integer.valueOf(prestamoClass.getnCarnet());
        int existePrestamo = 0;//control para la existencia de un numero de carnet    
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT id FROM prestamos_libros WHERE isbn = " + Isbn + " AND f_devolucion IS NULL ");
        while (resul.next()) {
            //si existe algun numero de carnet       
            existePrestamo = resul.getRow();
            return true;
        }
        if (existePrestamo == 0) {
            return false;
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return false;
    }

    /**
     * Existencia de devolucion de Book<br>
     * Metodo de confirmacion de la existencia de la devolucion de un libro o no
     *
     * @param prestamoClass parametro que contendra un objeto de la clase
     * PrestamoClass
     * @return retorna si la consulta conteniene alguna respuesta o no
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isBookDevuelto(PrestamoClass prestamoClass) throws SQLException {
//        System.out.println("dentro de isBookPrestado");
        int Isbn = Integer.valueOf(prestamoClass.getIsbn());
        int nCarnet = Integer.valueOf(prestamoClass.getnCarnet());
        int existePrestamo = 0;//control para la existencia de un numero de carnet    
        sentencia = (Statement) conn.createStatement();
        resul = sentencia.executeQuery("SELECT id FROM prestamos_libros WHERE isbn = " + Isbn + " AND carnet_usuario = " + nCarnet + " AND f_devolucion IS NOT NULL ");
        while (resul.next()) {
            //si existe algun numero de carnet       
            existePrestamo = resul.getRow();
            return true;
        }
        if (existePrestamo == 0) {
            return false;
        }
        resul.close();// Cerrar ResultSet
        sentencia.close();// Cerrar Statement
        return false;
    }

// END --   METODOS PARA LA TABLA PRESTAMOS_LIBROS      
//----------------------------------------------------------------------    
// METODOS PARA EL PRESTAMO DE LIBROS   
    /**
     * Actualizar prestamo de un libro<br>
     * Metodo que actualiza exclusivamente el campo carnet_usuario y con ello se
     * actualiza el estado del prestamo.
     *
     * @param isbn parametro cuyo valor correspondera con el numero de ISBN de
     * un libro
     * @param numCarnet parametro cuyo valor correspondera con el numero de
     * carnet del usuario
     * @throws SQLException Excepcion de tipo SQL
     */
    public void lendingBook(String isbn, String numCarnet) throws SQLException {
        int numIsbn = Integer.parseInt(isbn);
        int nCarnet = Integer.parseInt(numCarnet);
        // the mysql insert statement
        String query = "UPDATE libro SET carnet_usuario=? WHERE isbn=?";
        try ( // create the mysql insert preparedstatement
                PreparedStatement preparedStmt = (PreparedStatement) conn.prepareStatement(query)) {
            preparedStmt.setInt(1, nCarnet);
            preparedStmt.setInt(2, numIsbn);
            // execute the preparedstatement
            preparedStmt.execute();
            preparedStmt.close();
        }
    }

// END --  METODOS PARA EL PRESTAMO DE LIBROS  
    //INSERT REGISTROS TABLAS
    /**
     * Metodo que contiene la instruccion SQL de insertar datos de ejemplo
     * @return String 
     */
    public String getRowExampleInsertLibros() {

        return "INSERT INTO libro (id, isbn, titulo, editorial, carnet_usuario) VALUES\n"
                + "(1, 345047656, '  Argentina, Legend and History ', 'Morbi Quis Industries', NULL),\n"
                + "(2, 278652917, '  Catálogo Monumental de España; Provincia de Álava ', 'Nonummy Ut LLP', NULL),\n"
                + "(3, 790526971, '  Cuentos de mi tiempo ', 'Iaculis Odio Nam Consulting', NULL),\n"
                + "(4, 986388808, '  Don Francisco de Quevedo: Drama en Cuatro Actos ', 'Cras Corp.', NULL),\n"
                + "(5, 638601692, '  Bailén ', 'Proin Industries', NULL),\n"
                + "(6, 687295211, '  Diario de la expedicion de 1822 a los campos del sur de Buenos Aires ', 'am Scelerisque Company', NULL),\n"
                + "(7, 916697045, '  Algo de todo ', 'Tincidunt Associates', NULL),\n"
                + "(8, 861287660, '  Doña Clarines y Mañana de Sol ', 'Rutrum Urna Nec LLC', NULL),\n"
                + "(9, 9554954, '  Carlos Broschi ', 'At Company', NULL),\n"
                + "(10, 465860193, '  Diario de un viage a la costa de la mar Magallanica ', 'Imperdiet Associates', NULL),\n"
                + "(11, 993692173, '  Clásicos Castellanos: Libro de Buen Amor ', 'Ligula am PC', NULL),\n"
                + "(12, 65654911, '  El Diablo Cojuelo ', 'Nibh Phasellus Inc.', NULL),\n"
                + "(13, 669645239, '  Cuentos de Amor de Locura y de Muerte ', 'Mi Felis Institute', NULL),\n"
                + "(14, 315169563, '  De Las Islas Filipinas ', 'Sit Amet Ultricies Incorporated', NULL),\n"
                + "(15, 941646577, '  Ariel ', 'Orci Industries', NULL),\n"
                + "(16, 360387209, '  El derecho internacional americano ', 'Venenatis A LLP', NULL),\n"
                + "(17, 838353134, '  Amaury ', 'Ac Urna Inc.', NULL),\n"
                + "(18, 590769696, '  Cuentos de mi tiempo ', 'Enim Corporation', NULL),\n"
                + "(19, 376056175, '  Cocina del tiempo, ó arte de preparar sabrosos y exquisitos platos propios de cada estación ', 'Vel Arcu Eu Industries', NULL),\n"
                + "(20, 26659155, '40 años de proyecto Gútenberg ', 'Mauris Integer Sem Company', NULL),\n"
                + "(21, 436019736, '  De Las Islas Filipinas ', 'Dictum Foundation', NULL),\n"
                + "(22, 11525384, '  Comedias: El remedio en la desdicha; El mejor alcalde, el rey ', 'Luctus Limited', NULL),\n"
                + "(23, 618299909, '  Algo de todo ', 'Mi Ac Mattis Corp.', NULL),\n"
                + "(24, 912195095, '  Cuentos de Amor de Locura y de Muerte ', 'Malesuada Consulting', NULL),\n"
                + "(25, 65612827, '  Catálogo Instructivo de las Colecciones Mineralógicas ', 'Lacus Corporation', NULL),\n"
                + "(26, 224464996, '  Bocetos californianos ', 'Imperdiet Ornare Associates', NULL),\n"
                + "(27, 960106817, '  Cádiz ', 'Nisi Nibh Industries', NULL),\n"
                + "(28, 824051701, '  A First Spanish Reader ', 'Rutrum LLC', NULL),\n"
                + "(29, 552420984, '  Amaury ', 'Blandit Viverra Donec Industries', NULL),\n"
                + "(30, 331285868, '  Del libro impreso al libro digital ', 'Felis Limited', NULL),\n"
                + "(31, 492173809, '  Diario de la navegacion emprendida en 1781 ', 'Duis Sit Ltd', NULL),\n"
                + "(32, 581620380, '  Consideraciones Sobre el Origen del Nombre de los Números en Tagalog ', 'Fusce Dolor Company', NULL),\n"
                + "(33, 928277064, '  Doctrina Christiana ', 'Blandit Enim Consequat Foundation', NULL),\n"
                + "(34, 87087249, '  Cocina del tiempo, ó arte de preparar sabrosos y exquisitos platos propios de cada estación ', 'Non Institute', NULL),\n"
                + "(35, 486598757, '  El cocinero de su majestad ', 'a Limited', NULL),\n"
                + "(36, 737779872, '  Derroteros y viages à la Ciudad Encantada, ó de los Césares. ', 'Ullamcorper Nisl Incorporated', NULL),\n"
                + "(37, 245579672, '  El Estudiante de Salamanca and Other Selections ', 'Integer Vulputate Risus Ltd', NULL),\n"
                + "(38, 769814884, '  Aguas fuertes ', 'A Neque am PC', NULL),\n"
                + "(39, 461823262, '  Al primer vuelo ', 'Ornare Lectus Inc.', NULL),\n"
                + "(40, 347730822, '  Del libro impreso al libro digital ', 'Praesent Foundation', NULL),\n"
                + "(41, 8134981, '  Bocetos californianos ', 'Mauris Vel Incorporated', NULL),\n"
                + "(42, 298202710, '40 años de proyecto Gútenberg ', 'Dictum Proin Limited', NULL),\n"
                + "(43, 236527266, '  El Proyecto Gutenberg (1971-2009) ', 'Lorem Semper Institute', NULL),\n"
                + "(44, 350318485, '  Cuentos de Amor de Locura y de Muerte ', 'Odio Vel Est Associates', NULL),\n"
                + "(45, 253329173, '  El Manuscrito de mi madre ', 'Non Inc.', NULL),\n"
                + "(46, 594378824, '  Biografia de Simon Bolívar ', 'Pellentesque Sed Dictum Limited', NULL),\n"
                + "(47, 684053909, '  Cocina cómica ', 'Auctor Vitae Corp.', NULL),\n"
                + "(48, 583150645, '  El Mandarín ', 'In Incorporated', NULL),\n"
                + "(49, 381414172, '  Diccionario Bagobo-Español ', 'Magna Sed Institute', NULL),\n"
                + "(50, 25598225, '  Biografia de Simon Bolívar ', 'Ipsum Non Arcu Industries', NULL),\n"
                + "(51, 546739885, '  Diario de la navegacion emprendida en 1781 ', 'Sed LLP', NULL),\n"
                + "(52, 383936329, '  El Filibusterismo (Continuación del Noli me tángere) ', 'Ante Vivamus Foundation', NULL),\n"
                + "(53, 147136645, '  Curiosidades antiguas sevillanas ', 'Adipiscing Ltd', NULL),\n"
                + "(54, 631208684, '  Curiosidades antiguas sevillanas ', 'Sed Facilisis Industries', NULL),\n"
                + "(55, 283572561, '  Adriana Zumarán ', 'Egestas Institute', NULL),\n"
                + "(56, 825887747, '  Diario de la navegacion emprendida en 1781 ', 'Pulvinar Arcu Et Inc.', NULL),\n"
                + "(57, 529489575, '  Descripción colonial, libro segundo (2/2) ', 'Suspendisse Limited', NULL),\n"
                + "(58, 505416947, '  El Estudiante de Salamanca and Other Selections ', 'Nec Institute', NULL),\n"
                + "(59, 431035304, '  Actas capitulares desde el 21 - 25 de mayo de 1810 en Buenos Aires ', 'At Libero Morbi Limited', NULL),\n"
                + "(60, 632680167, '  Correspondencia Oficial sobre la Demarcacion de Limites entre el Paraguay y el Brasil ', 'Porttitor Vulputate Limited', NULL),\n"
                + "(61, 772001052, '  Cuentos de mi tiempo ', 'Id Limited', NULL),\n"
                + "(62, 565267521, '  Diario de la navegacion emprendida en 1781 ', 'Consequat PC', NULL),\n"
                + "(63, 701791314, '  Catálogo de los Objetos Etnologicos y Arqueologicos Exhibidos por la Expedición Hemenway ', 'Et Nunc Quisque Corporation', NULL),\n"
                + "(64, 688318658, '  Carlos Broschi ', 'Nec Corporation', NULL),\n"
                + "(65, 500031892, '  El Capitán Veneno ', 'Vitae Sodales At Ltd', NULL),\n"
                + "(66, 496624101, '  Diccionario Bagobo-Español ', 'Sit Amet Industries', NULL),\n"
                + "(67, 46224201, '  El Mar ', 'Condimentum Eget Volutpat Industries', NULL),\n"
                + "(68, 395152509, '  El Abate Constantin ', 'Porttitor Tellus LLP', NULL),\n"
                + "(69, 736440331, '  Bocetos californianos ', 'Adipiscing Inc.', NULL),\n"
                + "(70, 322726734, '  Biografia de Simon Bolívar ', 'Vivamus Nisi Industries', NULL),\n"
                + "(71, 107381097, '  A vuela pluma ', 'Non Sapien PC', NULL),\n"
                + "(72, 709534563, '  Cuentos de Amor de Locura y de Muerte ', 'Sed Corp.', NULL),\n"
                + "(73, 585245206, '  Colección de viages y expediciónes à los campos de Buenos Aires y a las costas de Patagonia ', 'Ornare Facilisis Eget Corporation', NULL),\n"
                + "(74, 687392583, '  Cádiz ', 'Metus Vivamus Inc.', NULL),\n"
                + "(75, 97891472, '  El Jayón ', 'Sed Dolor Inc.', NULL),\n"
                + "(76, 834471844, '  Diario del viaje al rio Bermejo ', 'Congue In Scelerisque Limited', NULL),\n"
                + "(77, 28832471, '  Actas capitulares desde el 21 - 25 de mayo de 1810 en Buenos Aires ', 'Ut a Cras Inc.', NULL),\n"
                + "(78, 608968062, '  Cuentos de mi tiempo ', 'Lorem Foundation', NULL),\n"
                + "(79, 416264166, '  Doña Luz ', 'Lectus Corp.', NULL),\n"
                + "(80, 419400214, '  Cocina cómica ', 'Id Mollis Nec Foundation', NULL),\n"
                + "(81, 191743611, '  El cuarto poder ', 'Enim Ltd', NULL),\n"
                + "(82, 499382205, '  El cocinero de su majestad ', 'Pede Ac Associates', NULL),\n"
                + "(83, 620420949, '  El Mar ', 'Nisl LLC', NULL),\n"
                + "(84, 59705481, '  Descripción Geografica, Histórica y Estadística de Bolivia, Tomo 1. ', 'Iaculis Lacus Pede Company', NULL),\n"
                + "(85, 532326375, '  Angelina ', 'Sem Vitae Corp.', NULL),\n"
                + "(86, 260530530, '  Don Quijote ', 'Auctor Company', NULL),\n"
                + "(87, 362348970, '  Diario de un reconocimiento de la guardia y fortines ', 'Quis Tristique Ac LLC', NULL),\n"
                + "(88, 552410212, '  Adriana Zumarán ', 'Mauris Inc.', NULL),\n"
                + "(89, 750440999, '  El Mandarín ', 'Scelerisque Dui LLP', NULL),\n"
                + "(90, 312214648, '  Dulce y sabrosa ', 'Eget Limited', NULL),\n"
                + "(91, 878482715, '  El Proyecto Gutenberg (1971-2009) ', 'A PC', NULL),\n"
                + "(92, 571126883, '  A vuela pluma ', 'Morbi Tristique Senectus Corporation', NULL),\n"
                + "(93, 129558179, '  Catálogo de los Objetos Etnologicos y Arqueologicos Exhibidos por la Expedición Hemenway ', 'Sociis Natoque Penatibus Corporation', NULL),\n"
                + "(94, 252085857, '  Booknología: El libro digital (1971-2010) ', 'Porttitor Eros Nec PC', NULL),\n"
                + "(95, 576564616, '  Diccionario Ingles-Español-Tagalog ', 'Etiam Imperdiet Dictum Institute', NULL),\n"
                + "(96, 8304961, '  Diario de un viage a la costa de la mar Magallanica ', 'Ac Eleifend Foundation', NULL),\n"
                + "(97, 232075676, '  El cuarto poder ', 'Orci In Corp.', NULL),\n"
                + "(98, 81905180, '  Antonio Azorín ', 'Dictum Limited', NULL),\n"
                + "(99, 915823211, '  Bailén ', 'Aliquam Rutrum Lorem Ltd', NULL),\n"
                + "(100, 541507759, '  El Gaucho Martín Fierro ', 'Lectus Pede Et Corporation', NULL),\n"
                + "(105, 1234567, 'pru4eba', 'preuba56', 0)";

    }

    /**
     * Metodo que contiene la instruccion SQL de insertar datos de ejemplo
     * @return String
     */
    public String getRowExampleInsertUsuarios() {
        return "INSERT INTO usuario (id, carnet, nombre, apellidos, direccion, telefono, email, password) VALUES\n"
                + "(11, 10000, 'Perry', 'Benton', 'Apartado núm.: 938, 5724 In C.', '034641365861', 'pede@idmagna.com', 'VIO76SQG4TQ'),\n"
                + "(12, 10001, 'Nicholas', 'Johnson', 'Apdo.:402-5743 Orci C.', '034364467576', 'feugiat@liberoat.edu', 'DCC77YGH3RZ'),\n"
                + "(13, 10002, 'Dieter', 'Hardy', 'Apartado núm.: 308, 9318 Justo Avda.', '034560549661', 'risus.Morbi@ipsumleoelementum.ca', 'CBR34VED7ZQ'),\n"
                + "(14, 10003, 'Azalia', 'Santiago', '263-2723 Ut Carretera', '034976869489', 'Praesent.eu@erat.ca', 'IIT27WDS6YL'),\n"
                + "(15, 10004, 'Mason', 'Underwood', '9389 Tempus ', '034599956644', 'malesuada@laoreet.ca', 'CPT54CSK9VX'),\n"
                + "(16, 10005, 'Wynter', 'Mcmahon', 'Apdo.:447-6943 Aliquet, Ctra.', '034184684662', 'mi@feugiat.edu', 'NLG95UMM9GY'),\n"
                + "(17, 10006, 'Ian', 'Kinney', 'Apartado núm.: 739, 6754 At, C/', '034658103507', 'Aliquam.auctor@nec.net', 'VEJ03VLS8GX'),\n"
                + "(18, 10007, 'Russell', 'Richards', '145-717 Magna. Calle', '034428913773', 'enim.sit@natoque.net', 'FOR20ERW9KQ'),\n"
                + "(19, 10008, 'Hector', 'Boone', 'Apdo.:333-7466 Consectetuer C/', '034644949799', 'Cum.sociis.natoque@QuisquevariusNam.edu', 'WZA52WLA2FO'),\n"
                + "(20, 10009, 'Alexa', 'Bennett', '464-5351 Nec, C.', '034750294647', 'gravida.non.sollicitudin@ametconsectetuer.co.uk', 'GAK51JBD1IW'),\n"
                + "(21, 10010, 'Gregory', 'Sanders', '5498 Cursus C.', '034264511182', 'eu@ridiculusmus.com', 'USA12EEZ6HE'),\n"
                + "(22, 10011, 'Jonas', 'Mayer', 'Apdo.:934-7995 Eros. Avenida', '034837541398', 'tempus@aclibero.ca', 'XMH25RRQ0ZF'),\n"
                + "(23, 10012, 'Lacota', 'Castro', '627-9699 Aliquet ', '034740778328', 'non.lorem.vitae@malesuadaaugueut.edu', 'QJO93KOH1MN'),\n"
                + "(24, 10013, 'Baxter', 'Downs', '4390 Sit Carretera', '034832578610', 'ac@nonhendrerit.net', 'MEL89UOU8IB'),\n"
                + "(25, 10014, 'Ferriss', 'Barry', '8879 Quisque Avenida', '034978543608', 'risus.a.ultricies@Sedpharetra.edu', 'YFB08RUG4MV'),\n"
                + "(26, 10015, 'Brett', 'Greene', 'Apdo.:265-1069 Dapibus ', '034478955101', 'Sed.pharetra.felis@liberoProinsed.org', 'OWV61IPC1YZ'),\n"
                + "(27, 10016, 'Lane', 'Gordon', 'Apartado núm.: 856, 4920 Et, Avda.', '034209125220', 'neque.pellentesque.massa@ullamcorper.net', 'TAX23NSC9RM'),\n"
                + "(28, 10017, 'Declan', 'Mccray', 'Apartado núm.: 779, 1458 Diam. ', '034906206290', 'fames@idliberoDonec.net', 'POS77NJY9IY'),\n"
                + "(29, 10018, 'Leilani', 'Nolan', 'Apartado núm.: 515, 3699 Dictum C.', '034918862257', 'imperdiet.erat.nonummy@a.edu', 'LAM61QWO5FR'),\n"
                + "(30, 10019, 'Reece', 'Bauer', 'Apdo.:584-4308 Nibh. Ctra.', '034607502344', 'vestibulum.neque.sed@Quisqueliberolacus.edu', 'LHR07HSM7FW'),\n"
                + "(31, 10020, 'McKenzie', 'Mclean', '765-5864 Nisi Av.', '034477577668', 'vulputate.risus@Aliquam.com', 'EOD75OFR5ES'),\n"
                + "(32, 10021, 'Christopher', 'Benton', 'Apartado núm.: 723, 8470 Sociis C.', '034645126324', 'natoque.penatibus@nonlaciniaat.co.uk', 'CCB13RJO2SX'),\n"
                + "(33, 10022, 'Nell', 'Vinson', 'Apartado núm.: 100, 3149 Id, Avda.', '034136604403', 'consequat.purus@fermentumvel.ca', 'RXP12OAX3UU'),\n"
                + "(34, 10023, 'Barrett', 'Obrien', 'Apdo.:686-1003 Augue Calle', '034874145370', 'Sed.eu.eros@dignissimmagnaa.org', 'LDZ98DDE1FW'),\n"
                + "(35, 10024, 'Yuri', 'Puckett', 'Apdo.:372-7470 Luctus Av.', '034827946149', 'vel.venenatis.vel@nisi.net', 'ZXV60RHF5GN'),\n"
                + "(36, 10025, 'Rose', 'Wong', 'Apartado núm.: 380, 8425 Lobortis. Av.', '034708380924', 'neque.Nullam@sit.edu', 'SSR15BAC8DL'),\n"
                + "(37, 10026, 'Pandora', 'Crawford', '3787 Id, Calle', '034428949169', 'congue.In@natoquepenatibus.edu', 'JDP81XLP2YZ'),\n"
                + "(38, 10027, 'Azalia', 'Cantu', '125-2571 Nisi Ctra.', '034961540270', 'vestibulum.neque.sed@conubia.edu', 'ZJA16EEN9HQ'),\n"
                + "(39, 10028, 'Harlan', 'Stevens', '223-6474 Adipiscing. C.', '034728336647', 'nec.malesuada.ut@nonummyipsumnon.ca', 'UXC73XJY1AU'),\n"
                + "(40, 10029, 'Indigo', 'Orr', 'Apdo.:649-945 Lorem, C.', '034634980638', 'vitae.nibh@lacus.com', 'FVA51YIE3QS'),\n"
                + "(41, 10030, 'Quentin', 'Knapp', 'Apartado núm.: 837, 7725 Ridiculus Avda.', '034575415503', 'Sed.molestie.Sed@Phasellusornare.edu', 'JIF32EMZ3AD'),\n"
                + "(42, 10031, 'Giacomo', 'Price', '3507 Sociis Calle', '034331381270', 'blandit.Nam.nulla@iaculis.ca', 'XTB37PLN0PC'),\n"
                + "(43, 10032, 'Dale', 'Burke', '895 Ullamcorper. ', '034881155912', 'orci@cursusin.ca', 'ZEE97GPL5GL'),\n"
                + "(44, 10033, 'Emmanuel', 'Barlow', '3497 Eleifend, Avenida', '034268613319', 'vestibulum.Mauris.magna@acfermentum.net', 'GJU82CAJ7JU'),\n"
                + "(45, 10034, 'Bert', 'Howard', '9838 Dui C.', '034976531291', 'egestas.Duis@semper.org', 'CFC57TOV9LZ'),\n"
                + "(46, 10035, 'Cameron', 'Randolph', '443-858 Est Carretera', '034325781923', 'non@tincidunt.ca', 'ZSV47GDA0ZU'),\n"
                + "(47, 10036, 'Nina', 'Wells', 'Apdo.:140-6917 Est. ', '034438905697', 'nisi.nibh@portaelita.edu', 'CYS03MLZ1ZB'),\n"
                + "(48, 10037, 'Victor', 'Rasmussen', '246-4948 Ut Avda.', '034905964417', 'malesuada@nuncullamcorpereu.com', 'ICG18LKL5IQ'),\n"
                + "(49, 10038, 'Elton', 'Molina', '287-8021 Aliquet. Avda.', '034636855203', 'ipsum.Donec.sollicitudin@Cumsociis.com', 'HJU96BNO0IS'),\n"
                + "(50, 10039, 'Sasha', 'Montgomery', 'Apartado núm.: 211, 3652 Nulla Calle', '034934444586', 'Nulla@arcuCurabiturut.ca', 'CIX45OHA0SN'),\n"
                + "(51, 10040, 'Amos', 'Stein', '8057 Malesuada Avenida', '034770657747', 'Sed@justofaucibus.org', 'CEK57OYR8CD'),\n"
                + "(52, 10041, 'Acton', 'Sandoval', 'Apartado núm.: 390, 5312 Pede. Avenida', '034769615983', 'vestibulum.massa@fringillaDonecfeugiat.com', 'DYE60FQJ2XZ'),\n"
                + "(53, 10042, 'Fiona', 'Tucker', '2183 Sollicitudin Carretera', '034438741549', 'eget.ipsum.Donec@eutellus.org', 'JPR23GPU1VL'),\n"
                + "(54, 10043, 'McKenzie', 'Chase', '8143 A Calle', '034644850716', 'Etiam.imperdiet.dictum@etultricesposuere.org', 'AJI93EXX2LS'),\n"
                + "(55, 10044, 'Kitra', 'Morgan', 'Apdo.:903-1010 Tincidunt Ctra.', '034862370464', 'metus@pedeblanditcongue.co.uk', 'LXI57TBE6DV'),\n"
                + "(56, 10045, 'Kelsie', 'Harrell', 'Apdo.:292-235 Dapibus Calle', '034644771877', 'tellus@Nullatinciduntneque.com', 'QIG53STS4IU'),\n"
                + "(57, 10046, 'Mari', 'Navarro', '951-3742 Risus. Avenida', '034115698705', 'eu.odio.tristique@egestasa.com', 'UFP59LBA3PG'),\n"
                + "(58, 10047, 'Bruno', 'Henson', '221 Ultrices. Avenida', '034479612485', 'natoque.penatibus.et@metus.ca', 'KHW72VEE4CF'),\n"
                + "(59, 10048, 'Camille', 'Hooper', '1985 Vel C/', '034488933204', 'nisi.Mauris.nulla@Phasellusin.edu', 'SJQ75EPF4KC'),\n"
                + "(60, 10049, 'Audrey', 'Holcomb', 'Apartado núm.: 579, 2585 Orci, Carretera', '034843458941', 'sit.amet@malesuadavel.edu', 'RCQ25MRL2HH'),\n"
                + "(61, 10050, 'Connor', 'Aguilar', 'Apartado núm.: 941, 3524 Non, Ctra.', '034608404410', 'est.Nunc@Duis.co.uk', 'ZEW29GSF1UN'),\n"
                + "(62, 10051, 'Mufutau', 'Hopper', '307-4670 Enim Calle', '034141942346', 'felis.Donec@euaugueporttitor.co.uk', 'TVA09JXB6NV'),\n"
                + "(63, 10052, 'Quynn', 'Dennis', '487-9785 Maecenas C.', '034460347859', 'libero.et@nondui.co.uk', 'GYI65KLB7QK'),\n"
                + "(64, 10053, 'Ryan', 'Diaz', 'Apdo.:799-9203 Eget Ctra.', '034493819291', 'Donec@Integervitaenibh.com', 'BAK23ZFY1OA'),\n"
                + "(65, 10054, 'Dolan', 'Ortiz', 'Apartado núm.: 937, 6929 Morbi Avda.', '034389204929', 'justo@et.net', 'STH81GOC6HD'),\n"
                + "(66, 10055, 'Emery', 'James', 'Apartado núm.: 553, 7298 Proin Av.', '034458186735', 'mauris@tellussem.com', 'XJR54VAH6VP'),\n"
                + "(67, 10056, 'Shellie', 'Kent', 'Apartado núm.: 139, 4653 Cursus Avenida', '034874614556', 'est.Mauris.eu@elitpellentesquea.com', 'GZN03UHI5GM'),\n"
                + "(68, 10057, 'Scarlett', 'Good', 'Apartado núm.: 912, 3555 Pede Carretera', '034939265811', 'dictum.eleifend.nunc@vitaedolor.com', 'KOJ58CHD3ZU'),\n"
                + "(69, 10058, 'Clayton', 'Wiley', '629-8587 Cras Avenida', '034847381208', 'et.magnis@convallis.co.uk', 'FUV51JVN8HA'),\n"
                + "(70, 10059, 'Caldwell', 'Dale', '7575 Dui Av.', '034746937550', 'Donec.nibh@atrisus.co.uk', 'KOX81POH5WY'),\n"
                + "(71, 10060, 'Elliott', 'Poole', '866-2189 Magna. Av.', '034550736714', 'ullamcorper@nuncrisusvarius.ca', 'EGC84PYN1VA'),\n"
                + "(72, 10061, 'Dillon', 'Stafford', 'Apdo.:569-2331 Duis Carretera', '034697271277', 'nascetur.ridiculus.mus@nuncIn.org', 'BOB41MXO7HO'),\n"
                + "(73, 10062, 'Amy', 'Bass', 'Apdo.:698-2782 Ligula. C/', '034918144135', 'amet@sitamet.ca', 'YXC69VPN6VW'),\n"
                + "(74, 10063, 'Ferris', 'Johnson', '362-4178 Sodales C/', '034252755141', 'accumsan.interdum.libero@massaInteger.co.uk', 'HAK29YRG8NE'),\n"
                + "(75, 10064, 'Travis', 'Tillman', 'Apartado núm.: 816, 2118 Vel, Calle', '034910196439', 'velit.Aliquam@nec.co.uk', 'DHB19CQL5KS'),\n"
                + "(76, 10065, 'Nissim', 'Nieves', 'Apartado núm.: 348, 277 Elementum, Avenida', '034190880275', 'velit.egestas@nectellusNunc.org', 'HMV55NXL3VO'),\n"
                + "(77, 10066, 'Zoe', 'Stephenson', 'Apartado núm.: 115, 5752 Nibh Carretera', '034872332451', 'aliquet.molestie@pharetrafeliseget.ca', 'GGT82XUL8VK'),\n"
                + "(78, 10067, 'Arthur', 'Torres', '8917 Vel Avda.', '034759513223', 'elit.dictum.eu@molestie.com', 'QFF12LIR6FD'),\n"
                + "(79, 10068, 'Indigo', 'Glenn', '5496 Et Avda.', '034490154699', 'at@porttitorvulputate.edu', 'IAN38YKT4UA'),\n"
                + "(80, 10069, 'Aquila', 'Gray', '679-3208 Semper ', '034987189309', 'a@necorci.co.uk', 'LQU20WPE4IT'),\n"
                + "(81, 10070, 'Brooke', 'Alford', '908-1760 Mi C.', '034203151778', 'dapibus.gravida.Aliquam@tortorIntegeraliquam.edu', 'JVY79WJR1ZX'),\n"
                + "(82, 10071, 'Matthew', 'Mcdonald', '829-1057 Placerat Av.', '034328724861', 'accumsan.neque.et@enimmi.co.uk', 'ZYC59CXW3GQ'),\n"
                + "(83, 10072, 'Blake', 'Elliott', 'Apdo.:192-6221 Nec, Calle', '034982834894', 'ac.orci.Ut@mattis.net', 'EVH37TLA5NU'),\n"
                + "(84, 10073, 'Eliana', 'Hayes', 'Apdo.:187-5219 Tristique ', '034120121839', 'felis.Nulla@arcu.co.uk', 'WQT44DKW9QY'),\n"
                + "(85, 10074, 'Hoyt', 'Rose', 'Apdo.:743-4669 Mauris, Av.', '034336142150', 'mauris.Suspendisse.aliquet@rutrum.org', 'PTD19GHC8UM'),\n"
                + "(86, 10075, 'Taylor', 'Macias', 'Apdo.:442-655 Etiam C/', '034559131753', 'auctor.velit.Aliquam@vitae.com', 'NEF76LHW1EH'),\n"
                + "(87, 10076, 'Lucius', 'Rose', 'Apartado núm.: 358, 1564 Curabitur ', '034899865892', 'interdum.enim.non@Suspendissesagittis.co.uk', 'MOC13HVV8GX'),\n"
                + "(88, 10077, 'Dominic', 'Kirkland', 'Apdo.:529-4646 Aliquet ', '034674792827', 'velit@vitaesemperegestas.com', 'WFA13YSZ6EY'),\n"
                + "(89, 10078, 'Skyler', 'Hawkins', 'Apdo.:205-3952 Vel C/', '034433354212', 'arcu@convallisantelectus.ca', 'ADK05KZG5GI'),\n"
                + "(90, 10079, 'Adrienne', 'Finch', '326-2618 Tempus, C/', '034140338754', 'mauris.erat.eget@elit.edu', 'QKC64SHY8OD'),\n"
                + "(91, 10080, 'Aurora', 'Terry', 'Apdo.:990-5211 Cursus. Avenida', '034360439761', 'euismod.enim@inconsequat.org', 'OLO14IWD3FS'),\n"
                + "(92, 10081, 'Leilani', 'Mitchell', 'Apartado núm.: 789, 7391 Iaculis Avenida', '034582687234', 'pulvinar.arcu.et@pede.com', 'PVI48USK1OJ'),\n"
                + "(93, 10082, 'Octavius', 'Hester', 'Apartado núm.: 881, 2986 Ante Avenida', '034901740845', 'montes.nascetur@insodaleselit.ca', 'GQR62JNP3MS'),\n"
                + "(94, 10083, 'Ria', 'Long', 'Apdo.:721-9680 Arcu Ctra.', '034314638383', 'id.erat@tristiquepharetraQuisque.edu', 'CUP96WPJ3PG'),\n"
                + "(95, 10084, 'Gray', 'Molina', 'Apartado núm.: 774, 5987 Eros C.', '034826868616', 'est.Mauris@tortorNunccommodo.net', 'BLK42QXT7BB'),\n"
                + "(96, 10085, 'Debra', 'Dunlap', '729-9190 Lacinia ', '034927627911', 'ipsum@massa.org', 'VTC69EDR5OX'),\n"
                + "(97, 10086, 'Amaya', 'Colon', 'Apdo.:305-3983 Suspendisse Avenida', '034304448756', 'lorem@dolor.ca', 'NIZ32ZQF6HD'),\n"
                + "(98, 10087, 'Otto', 'Burgess', 'Apdo.:548-5910 Amet, Avenida', '034471115582', 'id.ante.dictum@orcisem.edu', 'TMA89TTS4LC'),\n"
                + "(99, 10088, 'Kathleen', 'Hardin', 'Apartado núm.: 605, 8053 Metus Calle', '034315761137', 'metus.In.lorem@perconubia.org', 'YSB78EHS2FH'),\n"
                + "(100, 10089, 'Shad', 'Petersen', '741-8054 Vehicula Calle', '034519668511', 'ornare.tortor@eget.net', 'QDT56VOH0TX'),\n"
                + "(101, 10090, 'Adria', 'Glass', '979-1576 Cras Carretera', '034283587280', 'et.magna.Praesent@ante.com', 'WSC08WHH3BZ'),\n"
                + "(102, 10091, 'Regina', 'Wooten', '4163 Velit Avda.', '034310929567', 'viverra.Maecenas@montes.com', 'HFZ08QZJ8BR'),\n"
                + "(103, 10092, 'Kylynn', 'Adams', 'Apdo.:213-8506 Vestibulum Carretera', '034933469763', 'Nam.nulla.magna@tempor.net', 'BGL17QCT4XB'),\n"
                + "(104, 10093, 'Guy', 'Bolton', 'Apdo.:359-313 Semper Av.', '034672190995', 'amet@Maurisvestibulum.ca', 'ULV28LEK9AY'),\n"
                + "(105, 10094, 'Aurelia', 'Long', 'Apartado núm.: 295, 2304 Diam. Avenida', '034358295569', 'Curabitur.ut.odio@odiovelest.net', 'RAT81YPU1KC'),\n"
                + "(106, 10095, 'Zeph', 'Robertson', '929-5035 Ipsum. Avenida', '034353889296', 'orci.Ut@facilisisnonbibendum.net', 'AVB19WSQ7ZF'),\n"
                + "(107, 10096, 'Aurelia', 'Marks', '194-1628 At, Av.', '034506594731', 'auctor.ullamcorper@sed.com', 'GXY51EDY4NB'),\n"
                + "(108, 10097, 'Kimberley', 'Pena', '8324 Praesent Carretera', '034746719601', 'magna@pedemalesuada.co.uk', 'ZMT93BXN7KN'),\n"
                + "(109, 10098, 'Hamilton', 'William', '5719 Dapibus Avenida', '034310630993', 'blandit@turpis.ca', 'MDK50JID5IL'),\n"
                + "(110, 10099, 'Carter', 'Beck', '180-1100 Sit Calle', '034954228557', 'enim.Nunc.ut@facilisiSed.edu', 'CUC01ENY3DX'),\n"
                + "(111, 123456, 'abraham', 'fernandez sanchez', 'plz san esteban nº 6', '679269317', 'brajan@brajan.com', '1234'),\n"
                + "(113, 654321, 'nombrePrueba', 'apellidosPrueba', 'calle mas', '456987456', 'nombrePrueba@com.com', '1111'),\n"
                + "(114, 1234567, 'nompru', 'apellpru', 'calle prui', '456987412', 'nompru@apellpru.com', '1111'),\n"
                + "(120, 12345678, 'a', 'a', 'a', '1', 'a', '23'),\n"
                + "(126, 32131, 'werwrwrew', 'rewrwe', 'rewrwe', '321', 'rwerwe', 'rwere'),\n"
                + "(127, 45234, 'trwetet', 'trete', 'trete', '432', 'rwere', 'rwe')";
    }

    /**
     * Metodo que contiene la instruccion SQL de insertar datos de ejemplo
     * @return String
     */
    public String getRowExampleInsertPrestamos() {

        return "INSERT INTO prestamos_libros (id, isbn, carnet_usuario, f_salida, f_devolucion) VALUES\n"
                + "(4, 1234567, 1234567, '2018-04-01', '2018-04-15'),\n"
                + "(5, 65612827, 10002, '2018-02-13', NULL),\n"
                + "(6, 912195095, 10002, '2018-01-12', NULL),\n"
                + "(7, 26659155, 10018, '2017-12-20', NULL),\n"
                + "(9, 65654911, 10005, '2017-11-16', NULL),\n"
                + "(10, 861287660, 10013, '2018-03-17', NULL),\n"
                + "(11, 986388808, 10010, '2018-02-16', NULL),\n"
                + "(17, 315169563, 10010, '2018-02-16', '2018-04-05')";
    }
}
