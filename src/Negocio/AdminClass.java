/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import DAO.ClaseBBDD;
import Vista.Escritorio;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Abraham Fernandez Sanchez
 */
public class AdminClass {

    ClaseBBDD bbdd;
    Escritorio escritorio;

    //------constructores de Clase
    public AdminClass() {
    }

    public AdminClass(Escritorio escritorio) {
        this.escritorio = escritorio;
    }

    // Constructor y Getters y Setters para el uso de los datos del admin
    private static String adminNow = "";
    private static String passwordNow = "";

    /**
     * Devuelve el administrador actual
     *
     * @return devulve String
     */
    public String getAdminNow() {
        return adminNow;
    }

    /**
     * Envia el administrador actual
     *
     * @param adminNow envia String
     */
    public void setAdminNow(String adminNow) {
        AdminClass.adminNow = adminNow;
    }

    /**
     * Devuelve el password actual
     *
     * @return passwordNow String
     */
    public String getPasswordNow() {
        return passwordNow;
    }

    /**
     * Envio del password actual
     *
     * @param passwordNow String
     */
    public void setPasswordNow(String passwordNow) {
        AdminClass.passwordNow = passwordNow;
    }

    //----- METODOS DE CLASE
    // Metodos de control de logeo del administrador
    private static boolean AdminLogeado = false;//false = administrador no logeado - True = administrador logeado

    /**
     * Devuelve si existe un administrador logeado o no
     *
     * @return true / false
     */
    public boolean isAdminLogeado() {
        return AdminLogeado;
    }

    /**
     * Envia el estado actual del administrador que este en el sistema
     *
     * @param AdminLogeado String
     */
    public void setAdminLogeado(boolean AdminLogeado) {
        AdminClass.AdminLogeado = AdminLogeado;
    }

    /**
     * Cambio de Password<br>
     * Metodo para el cambio de password del administrador
     *
     * @param newPass String
     * @throws SQLException Excepcion de tipo SQL
     */
    public void changePasswordAdmin(String newPass) throws SQLException {
        bbdd = new ClaseBBDD();
        bbdd.updatePasswordAdministrator(getAdminNow(), getPasswordNow(), newPass);
        setPasswordNow(newPass);
    }

    /**
     * Logeo del Administrador<br>
     * Metodo de login para los administradores del sistema
     *
     * @param nombre String Nombre del administrador
     * @param password String contraseña
     * @return booleano
     * @throws SQLException Excepcion de tipo SQL
     * @throws PropertyVetoException Excepcion de tipo PropertyVeto
     */
    public boolean loginAdmin(String nombre, String password) throws SQLException, PropertyVetoException {
        bbdd = new ClaseBBDD();
        return bbdd.isAdministrator(nombre, password);//
    }

    /**
     * Metodo que contiene todos los pasos para instalar la base de datos en MYSQL
     */
    public void instalacionBBDD() {

        try {
            bbdd = new ClaseBBDD();

            if (!bbdd.conexionMYSQL()) {
                JOptionPane.showMessageDialog(escritorio, "Ha ocurrido un error de conexión, es muy probable que no tenga encendido el servidor para MySql");  
                System.exit(0);
            } else {

                if (!bbdd.dbExists()) {

                    if (bbdd.creacionBBDD() && bbdd.dbExists()) {

                        String textoInstalacion = "Se ha detectado que no tiene instalada la base de datos necesaria para la utilizacion de este software. \n "
                                + "Para la instalacion es necesario que este funcionando el servidor. \n"
                                + "Se va ha iniciar la instalacion de la base de datos y las tablas que la componen";

                        JOptionPane.showMessageDialog(escritorio, textoInstalacion);
                        bbdd.creacionTablas();
                        JOptionPane.showMessageDialog(escritorio, "La base de datos se ha instalado correctamente");
                        instalacionDatosEjemplo();

                    } else {

                        JOptionPane.showMessageDialog(escritorio, "La base de datos no ha sido instalada, ha ocurrido algun error");
                    }

                } else {
                    System.out.println("existe");
//                showCreacionTablas();
                }
            }
        } catch (SQLException ex) {
            System.out.println("excepcion");
            JOptionPane.showMessageDialog(escritorio, "Ha ocurrido un error de conexión, es muy probable que no tenga encendido el servidor para MySql");
//            Logger.getLogger(AdminClass.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }

    /**
     * Metodo para la insercion de los datos de ejemplo en todas las tablas
     */
    public void instalacionDatosEjemplo() {
        int respuesta = JOptionPane.showConfirmDialog(null, "¿Quieres instalar registros de ejemplo para la base de datos ? ", "Alerta!", JOptionPane.YES_NO_OPTION);
        System.out.println(respuesta);
        if (respuesta == 0) {
            try {
                bbdd = new ClaseBBDD();
                bbdd.insercionDatosEjemplo();
                JOptionPane.showMessageDialog(escritorio, "Puede iniciar su sesion");

            } catch (SQLException ex) {
//                Logger.getLogger(AdminClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(escritorio, "Puede iniciar su sesion");
        }
    }

}
