/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Negocio;

import java.sql.Date;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class PrestamoClass {
    
    private int id;
    private String isbn;
    private String nCarnet;
    private java.sql.Date fechaSalida;
    private java.sql.Date fechaDevolucion;

    /**
     * Constructor de clase <br>
     * 
     * @param id integer identificador 
     * @param isbn String numero de referencia de libro
     * @param nCarnet Strin numero de carnet de un usuario
     * @param fechaSalida Date fecha de salida de prestamo
     * @param fechaDevolucion  Date fecha de devolucion de prestamo
     */
    public PrestamoClass(int id, String isbn, String nCarnet, Date fechaSalida, Date fechaDevolucion) {
        this.id = id;
        this.isbn = isbn;
        this.nCarnet = nCarnet;
        this.fechaSalida = fechaSalida;
        this.fechaDevolucion = fechaDevolucion;
    }
    
    /**
     * Constructor de clase <br>
     * @param isbn String numero de referencia de un libro
     * @param nCarnet String numero de carnet de un usuario
     * @param fechaSalida Date fecha de salida de prestamo
     * @param fechaDevolucion Date fecha de devolucion de prestamo
     */
    public PrestamoClass(String isbn, String nCarnet, Date fechaSalida, Date fechaDevolucion) {
        this.isbn = isbn;
        this.nCarnet = nCarnet;
        this.fechaSalida = fechaSalida;
        this.fechaDevolucion = fechaDevolucion;
    }

    /**
     * Constructor de clase <br>
     * @param isbn String numero de referencia de un libro
     * @param nCarnet String numero de carnet de un usuario
     * @param fechaSalida Date fecha de salida de un prestamo
     */
    public PrestamoClass(String isbn, String nCarnet, Date fechaSalida) {
        this.isbn = isbn;
        this.nCarnet = nCarnet;
        this.fechaSalida = fechaSalida;
    }
        
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getnCarnet() {
        return nCarnet;
    }

    public void setnCarnet(String nCarnet) {
        this.nCarnet = nCarnet;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Date getFechaDevolucion() {
        return fechaDevolucion;
    }

    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }

    public int getId() {
        return id;
    }
    
    
}
