/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Negocio;

/**
 * 
 * @author Abraham Fernandez Sanchez 
 */
public class BookClass {
    
    private  String isbn;
    private  String titulo;
    private  String editorial;
    private  int carnetUsuario;
    
    public BookClass(String isbn, String titulo, String editorial) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.editorial = editorial;
    }

    public BookClass() {}
        

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public int getCarnetUsuario() {
        return carnetUsuario;
    }

    public void setCarnetUsuario(int carnetUsuario) {
        this.carnetUsuario = carnetUsuario;
    }
    
    
 

}
