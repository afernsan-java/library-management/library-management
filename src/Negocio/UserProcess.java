/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import DAO.ClaseBBDD;
import Vista.Escritorio;
import java.awt.HeadlessException;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Abraham Fernandez Sanchez
 */
public class UserProcess {

    Escritorio escritorio;
    ClaseBBDD BBDD;
    UserClass user;

    final int NUM_CARNET_EXIST = 0;//el numero de carnet solicitado ya existe en la BBDD
    final int EMAIL_EXIST = 1;//el email solicitado ya existe en la BBDD
    final int DATA_EXIST = 2;//ambos datos solicitados ya existen en la BBDD
    final int NO_EXIST_DATA = 3;//no existe ningun dato como los solicitados en la BBDD 
    final int EXIST_USER = 4;// Existencia positiva de un usuario concreto
    final int NOT_EXIST_USER = 5;// Existencia NO positiva de un usuario concreto      

    /**
     * Añadir un nuevo usuario<br>
     * Metodo para añadir un nuevo usuario donde se pasaran los parametros
     * descritos en los atributos, para que pueda realizar la accion
     *
     * @param usuario objeto de la clase UserClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public void addUsuario(UserClass usuario) throws SQLException {
        user = usuario;
        String nCarnet = user.getNumCarnet();
        int RESPUESTA = (isNumCarnetExist(nCarnet)) ? EXIST_USER : NOT_EXIST_USER;
        
        if (confirmacionDeAccion()) {            
            
            if (RESPUESTA == NOT_EXIST_USER) {
                try {
                    String nombre = user.getNombre();
                    String apellidos = user.getApellidos();
                    String direccion = user.getDireccion();
                    String telefono = user.getTlf();
                    String email = user.getEmail();
                    String password = user.getPassword();    
                    responseDialogsUserAdd(nCarnet, email);
                    BBDD = new ClaseBBDD();
                    BBDD.insertUserBBDD(nCarnet, nombre, apellidos, direccion, telefono, email, password);
                    
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(escritorio, "Debes introducir datos correctos");
                }
            } else {
                JOptionPane.showMessageDialog(escritorio, "Este usuario ya existe");
            }
        }
    }

    /**
     * Actualizar usuario<br>
     * Metodo para actualizar los datos de un usuario concreto
     *
     * @param user Objeto de la clase UserClass
     * @throws java.sql.SQLException Excepcion de tipo SQL
     */
    public void actualizarUsuario(UserClass user) throws SQLException {
        if (confirmacionDeAccion()) {
            try {
                BBDD = new ClaseBBDD();
                BBDD.updateUserBBDD(
                        user.getNumCarnet(),
                        user.getNombre(),
                        user.getApellidos(),
                        user.getDireccion(),
                        user.getTlf(),
                        user.getEmail(),
                        user.getPassword()
                );
                JOptionPane.showMessageDialog(escritorio, "El usuario ha sido actualizado correctamente");
            } catch (HeadlessException | SQLException e) {
                JOptionPane.showMessageDialog(escritorio, "Ha ocurrido un error, vuelva autilizarlo mas adelante");
            }
        }
    }

    /**
     * Eliminacion de un usuario<br>
     * Metodo para eliminar a un usuario. Necesita el numero de carnet
     * "numCarnet" como parametro. En el caso de que la eliminacion sea correcta
     * o no, devolvera:<br>
     * final int EXIST_USER=1 = "El numero de carnet existe y ha sido
     * elimando"<br>
     * final int NOT_EXIST_USER=0 = "El numero de carnet no existe y no ha sido
     * posible elimarlo"
     *
     * @param numCarnet numero de carnet de la biblioteca del usuario
     */
    public void deleteUsuario(String numCarnet) {
        if (confirmacionDeAccion()) {
            try {
                BBDD = new ClaseBBDD();
                int RESPUESTA = (isNumCarnetExist(numCarnet)) ? EXIST_USER : NOT_EXIST_USER;
                if (RESPUESTA == EXIST_USER) {
                    BBDD.deleteUserBBDD(numCarnet);
                }
                responseDialogsUserDeleted(RESPUESTA);
            } catch (HeadlessException | SQLException e) {
                JOptionPane.showMessageDialog(escritorio, "No ha introducido un numero de carnet de usuario valido");
            }
        }
    }

    /**
     * Confirmacion de accion <br>
     *
     * @return booleano
     */
    public boolean confirmacionDeAccion() {
        int resp = JOptionPane.showConfirmDialog(null, "¿Esta seguro de realizar esta accion ? ", "Alerta!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        return resp == 0;
    }

    /**
     * Comprobar si existen actualmente un numero de carnet o una direccion
     * email<br>
     * Metodo para controlar la duplicidad de los datos que no pueden estar
     * duplicados
     *
     * @param carnet parametro correspondiente al numero de carnet de un usuario
     * @param email parametro correspondiente al email de un usuario
     * @throws SQLException Excepcion de tipo SQL
     */
    public void responseDialogsUserAdd(String carnet, String email) throws SQLException {
        if (isNumCarnetExist(carnet)) {
            JOptionPane.showMessageDialog(escritorio, "No se puede efectuar la grabacion: \nNumero de Carnet ya existe ");
        } else if (isEmailExist(email)) {
            JOptionPane.showMessageDialog(escritorio, "No se puede efectuar la grabacion: \nEmail ya existe ");
        } else if (isNumCarnetExist(carnet) && isEmailExist(email)) {
            JOptionPane.showMessageDialog(escritorio, "No se puede efectuar la grabacion: \nNumero de Carnet y el Email ya existen ");
        } else {
            JOptionPane.showMessageDialog(escritorio, "El usuario ha sido grabado correctamente");
        }
    }

    /**
     * Respuestas ' tipo ' de dialogo para la accion de eliminacion de un
     * usuario<br>
     *
     * Metodo que ejecutara las acciones suficientes para mostrar por panatalla
     * los mensajes de los errores o acciones correspondientes. NUM_CARNET_EXIST
     * = 0;//el numero de carnet solicitado ya existe en la BBDD<br>
     * EMAIL_EXIST = 1;//el email solicitado ya existe en la BBDD<br>
     * DATA_EXIST = 2;//ambos datos solicitados ya existen en la BBDD<br>
     * NO_EXIST_DATA = 3;//no existe ningun dato como los solicitados en la BBDD
     * <br>
     * EXIST_USER=4;// Existencia positiva de un usuario concreto<br>
     * NOT_EXIST_USER=5;// Existencia NO positiva de un usuario concreto   <br>
     *
     * @param RESPUESTA integer (Constantes)
     */
    public void responseDialogsUserDeleted(int RESPUESTA) {
        switch (RESPUESTA) {
            case EXIST_USER:
                JOptionPane.showMessageDialog(escritorio, "El usuario ha sido eliminado correctamente");
                break;
            case NOT_EXIST_USER:
                JOptionPane.showMessageDialog(escritorio, "No ha sido encontrado ningun usuario con este numero de carnet");
                break;
            default:
                JOptionPane.showMessageDialog(escritorio, "No se ha podido realizar ninguna operacion");
                break;
        }
    }

    /**
     * Obtener un usuario<br>
     * Metodo que devolvera en un objeto de la clase "UserClass", en el cual
     * estaran los datos individuales de un usuario en concreto.
     *
     * @param numCarnet parametro correspondiente al numero de carnet de un
     * usuario
     * @return retorna un objeto de la clase UserClass que contendra los datos
     * pertenecientes a este
     * @throws SQLException Excepcion de tipo SQL
     */
    public UserClass recoverObjUser(String numCarnet) throws SQLException {
        BBDD = new ClaseBBDD();
        //se obtiene un objeto de tipoUserClass con los atributos del usuario solicitado   
        return BBDD.getObjectUser(numCarnet);
    }

    /**
     * Obtencion de un usuario en concreto<br>
     *
     * Metodo para recuperar un objeto de la clase UserClass, este objeto estara
     * servido desde DAO, desde la clase ClaseBBDD
     *
     * @param nCarnet parametro correspondiente al numero de carnet de un
     * usuario
     * @return retorna un objeto de la clase UserClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public UserClass getUser(String nCarnet) throws SQLException {
        BBDD = new ClaseBBDD();
        return BBDD.getObjectUser(nCarnet);
    }

    /**
     * Comprobacion de la existencia de un numero de carnet.<br>
     *
     * @param numCarnet String numero de carnet de usuario
     * @return boolean
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isNumCarnetExist(String numCarnet) throws SQLException {
        BBDD = new ClaseBBDD();
        return BBDD.isUserCarnetNumber(numCarnet);
    }

    /**
     * Comprobacion de la existencia de un email.<br>
     *
     * @param email String direccion de email de usuario
     * @return boolean
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isEmailExist(String email) throws SQLException {
        BBDD = new ClaseBBDD();
        return BBDD.isUserEmail(email);
    }

}
