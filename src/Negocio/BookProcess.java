/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Negocio;

import DAO.ClaseBBDD;
import Vista.Escritorio;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * 
 * @author Abraham Fernandez Sanchez 
 */
public class BookProcess {
    
    ClaseBBDD BBDD;
    Escritorio escritorio;    
  
    /**
     * Añadir libro a la biblioteca<br>
     * 
     * @param isbn String numero referencia de un libro
     * @param titulo String nombre del libro
     * @param editorial String editorial del libro
     * @throws SQLException Excepcion de tipo SQL
     */
    public void addNewBook(String isbn, String titulo, String editorial) throws SQLException{        
        if(confirmacionDeAccion(isbn) && !isISBNExist(isbn)){
            int numIsbn= Integer.parseInt(isbn); 
            BBDD = new ClaseBBDD();
            BBDD.insertBookBBDD(numIsbn, titulo, editorial); 
            JOptionPane.showMessageDialog(escritorio, "El libro ha sido grabado correctamente");
        }else{
            JOptionPane.showMessageDialog(escritorio, "El libro ya existe en la base de datos, no se puede guardar");
        }    
    }
    
    /**
     * Actualizacion de los datos de un libro<br>
     * 
     * @param libro Objeto de la clase BookClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public void updateLibro(BookClass libro) throws SQLException{
        BBDD = new ClaseBBDD();       
        String numISBN = libro.getIsbn(); 
        if(confirmacionDeAccion(numISBN)){
            if(isISBNExist(String.valueOf(numISBN))){
                String titulo = libro.getTitulo();
                String editorial = libro.getEditorial();
                BBDD.updateBookBBDD(numISBN, titulo, editorial);    
                JOptionPane.showMessageDialog(escritorio, "El libro ha sido modificado correctamente");
            }else{
                JOptionPane.showMessageDialog(escritorio, "El libro no existe en la base de datos, no se puede guardar");
            }    
        }
    }
    
    /**
     * Borrado de un libro de la biblioteca<br>
     * 
     * @param numISBN String Numero referencia de un libro de la biblioteca
     * @throws SQLException Excepcion de tipo SQL
     */
    public void deleteBook(String numISBN) throws SQLException { 
        if(confirmacionDeAccion(numISBN)){
            if(isISBNExist(numISBN)){  
                BBDD = new ClaseBBDD();
                int nISBN = Integer.parseInt(numISBN);// paso de String a integer el num de carnet
                BBDD.deleteBookBBDD(nISBN);
                JOptionPane.showMessageDialog(escritorio, "Se ha eliminado correctamente el libro");
            }else if(!isISBNExist(numISBN)){
                 JOptionPane.showMessageDialog(escritorio, "No ha sido encontrado ningun libro con este numero ISBN");
            } 
        }
    } 

    
    /**
     * Existencia de un libro en la biblioteca<br>
     * 
     * @param numISBN String Numero referencia de un libro de la biblioteca
     * @return booleano
     */
    public boolean isISBNExist(String numISBN){
        try {
            BBDD = new ClaseBBDD();   
            return BBDD.isISBNExists(numISBN);            
        } catch (NumberFormatException | SQLException e) {
            return false;
        }
    }
    
    /**
     * Recuperacion de un libro determinado de la biblioteca<br>
     * 
     * @param numISBN  String Numero referencia de un libro de la biblioteca
     * @return retornara un Objeto de la clase BookClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public BookClass getBook(String numISBN) throws SQLException{            
        if(isISBNExist(numISBN)){
//            JOptionPane.showMessageDialog(escritorio, "El libro ha sido encontrado");
            return recoverObjBook(numISBN);
        }else{
//            JOptionPane.showMessageDialog(escritorio, "El libro No existe en la Base de Datos");
             return null;
        }       
    }    
    
    /**
     * Recuperacion de un libro determinado de la biblioteca<br>
     * 
     * @param numISBN String Numero referencia de un libro de la biblioteca
     * @return retornara un Objeto de la clase BookClass
     * @throws SQLException Excepcion de tipo SQL
     */
    public BookClass recoverObjBook(String numISBN) throws SQLException{        
        BBDD = new ClaseBBDD();
        return  BBDD.getObjectBook(numISBN);//se obtiene un objeto de tipo BookClass con los atributos del usuario solicitado 
    }
    
  
    
    /**
     * Confirmacion de accion a realizar<br>
     * 
     * @param numISBN String Numero referencia de un libro de la biblioteca
     * @return booleano
     * @throws java.sql.SQLException Excepcion de tipo SQL
     */
    public boolean confirmacionDeAccion(String numISBN) throws SQLException{  
//        BookClass book = recoverObjBook(numISBN);
        int resp = JOptionPane.showConfirmDialog(
                null, "¿Esta seguro de continuar ? \n","Alerta!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        return resp==0;
    }    
    
}
