/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Negocio;

import DAO.ClaseBBDD;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

/**
 * 
 * @author Abraham Fernandez Sanchez 
 */
public class PrestamosProcess {
   
    UserProcess userProcess;
    BookProcess bookProcess;    
    ClaseBBDD bbdd;
    PrestamoClass prestamoClass;
    
    public PrestamosProcess() {
    }

    
    /**
     * Prestamo de un libro<br>
     * 
     * Metodo para el prestamo por parte de un usuario de un libro.
     * 
     * @param ISBN parametro cuyo valor correspondera con el numero de ISBN  de un libro
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet del usuario
     * @return booleano
     * @throws SQLException Excepcion de tipo SQL
     * @throws java.text.ParseException  Excepcion de tipo parse
     */
    public boolean prestarLibro(String ISBN, String nCarnet) throws SQLException, ParseException{
        userProcess = new UserProcess();
        bookProcess = new BookProcess();        
        boolean isPrestado = isPrestadoBook(ISBN, nCarnet);
        boolean numCarnet = userProcess.isNumCarnetExist(nCarnet);
        boolean isIsbn = bookProcess.isISBNExist(ISBN);  
           
        if(!isPrestado && numCarnet && isIsbn ){      
            bbdd = new ClaseBBDD();  
            prestamoClass = new PrestamoClass(ISBN, nCarnet, getFechaActual());
            bbdd.lendingBook(ISBN, nCarnet);             
            bbdd.insertPrestamoLibroBBDD(prestamoClass);
            return true;
        }else{
            return false;
        }
    }
    
    /**
     * Devolucion de un libro<br>
     * 
     * Metodo para la devolucion por parte de un usuario de un libro.
     * 
     * @param ISBN parametro cuyo valor correspondera con el numero de ISBN  de un libro
     * @param nCarnet parametro cuyo valor correspondera con el numero de carnet del usuario
     * @return booleano
     * @throws SQLException  Excepcion de tipo SQL
     */
    public boolean devolverLibro(String ISBN, String nCarnet) throws SQLException{
        userProcess = new UserProcess();
        bookProcess = new BookProcess();        
        boolean isPrestado = isPrestadoBook(ISBN, nCarnet);
        boolean numCarnet = userProcess.isNumCarnetExist(nCarnet);
        boolean isIsbn = bookProcess.isISBNExist(ISBN);        
        if(isPrestado && numCarnet && isIsbn ){
            bbdd = new ClaseBBDD();
            bbdd.lendingBook(ISBN, "0");             
            prestamoClass = bbdd.getObjectPrestamo(ISBN, nCarnet);
            prestamoClass.setFechaDevolucion(getFechaActual()); 
            bbdd.finalizarPrestamoLibroBBDD(prestamoClass);
            return true;
        }else{
            return false;
        }  
    }
    
    /**
     * Obtencion de Objeto Date.sql<br>
     * 
     * Metodo que devolvera un objeto de la clase Date.sql
     * 
     * @return Retorna un objeto Date.sql con la fecha del dia en que se crea dicho objeto
     */
    public java.sql.Date getFechaActual(){
    java.util.Date utilDate = new java.util.Date();
    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
    System.out.println("java.util.Date: " + utilDate);
    System.out.println("java.sql.Date.: " + sqlDate);   
        return sqlDate;
    }   
    
    /**
     * Existencia de un prestamo <br>
     * 
     * Metodo para saber si un libro esta actualemente en prestamo introduciendo el numero
     * de ISBNy el numero de carnet
     * 
     * @param ISBN String parametro correspondiente al numero ISBN de un libro
     * @param nCarnet String numero de carnet de usuario
     * @return  Boolean
     * @throws SQLException  Excepcion de tipo SQL
     */
    public boolean isPrestadoBook(String ISBN, String nCarnet) throws SQLException{
        bbdd = new ClaseBBDD();
        prestamoClass = bbdd.getObjectPrestamo(ISBN, nCarnet);        
        return bbdd.isBookPrestado(prestamoClass);
    }
    
    /**
     * Comprobacion de la existencia de un libro concreto prestado <br>
     * Metodo para saber si un libro esta actualemente en prestamo introduciendo solamente el numero ISBN
     * 
     * @param ISBN String - Numero del ISBN de un libro
     * @return Boolean
     * @throws SQLException Excepcion
     */
    public boolean isPrestadoBookOnlyIsbn(String ISBN) throws SQLException{
        bbdd = new ClaseBBDD();
        prestamoClass = bbdd.getObjectPrestamo(ISBN, "1111");        
        return bbdd.isBookPrestado(prestamoClass);
    }
    
    /**
     * Responde si un libro se a devuelto <br>
     * 
     * @param ISBN String numero de referencia de un libro
     * @param nCarnet String numero de carnet de un usuario
     * @return booleano
     * @throws SQLException Excepcion de tipo SQL
     */
    public boolean isDevueltoBook(String ISBN, String nCarnet) throws SQLException{
        bbdd = new ClaseBBDD();
        prestamoClass = bbdd.getObjectPrestamo(ISBN, nCarnet);
        return bbdd.isBookDevuelto(prestamoClass);
    }    
    
    /**
     * Devulve un ArrayList con los prestamos actuales de libros de un usuario <br>
     * 
     * @param nCarnet String numero de carnet de usuario
     * @return ArrayList
     * @throws SQLException Excepcion de tipo SQL
     */
    public ArrayList showPrestamosActualesUsuario(String nCarnet) throws SQLException{
        bbdd = new ClaseBBDD();
      
        return bbdd.listadoPrestamosActualesUsuario(nCarnet);        
    }
    
    /**
     * Devulve un ArrayList con el historial de prestamos de libros de un usuario <br>
     * @param nCarnet String numero de carnet de usuario
     * @return ArrayList
     * @throws SQLException Excepcion de tipo SQL
     */
    public ArrayList showHistorialPrestamosUsuario(String nCarnet) throws SQLException{
        bbdd = new ClaseBBDD();
        return bbdd.listadoHistorialPrestamosUsuario(nCarnet);        
    }    


}
