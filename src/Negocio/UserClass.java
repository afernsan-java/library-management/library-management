package Negocio;

/** 
 * Clase UserClass<br>
 * 
 * Integra los constructores y metodos para la creacion y manipulacion de los datos de los usuarios que sean necesarios
 * 
 * @author Abraham Fernandez Sanchez 
 */
public class UserClass {
    
    private String numCarnet;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String tlf;
    private String email;
    private String password;

    /**
     * Constructor de clase con atributos de la clase
     * 
     * @param numCarnet numero del carnet
     * @param nombre nombre del usuario
     * @param apellidos apellidos del usuario
     * @param direccion direccion del usuario
     * @param tlf telefono del usuario
     * @param email email del usuario
     * @param password  contraseña del usuario
     */
    public UserClass(String numCarnet, String nombre, String apellidos, String direccion, String tlf, String email, String password) {
        this.numCarnet = numCarnet;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.tlf = tlf;
        this.email = email;
        this.password = password;
    }
    
    /**
     * Constructor de clase vacio
     */
    public UserClass(){ }
    
    /**
     * 
     * @return String - devulve el numero del carnet de usuario
     */
    public String getNumCarnet() {
        return numCarnet;
    }

    /**
     * 
     * @param numCarnet numero de carnet de usuario
     */
    public void setNumCarnet(String numCarnet) {
        this.numCarnet = numCarnet;
    }

    /**
     * 
     * @return String - devulve el nombre del usuario
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * 
     * @param nombre nombre del usuario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * 
     * @return String - devulve los apellidos del usuario
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * 
     * @param apellidos  apellidos del usuario
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * 
     * @return String - devulve la direccion 
     */
    public String getDireccion() {
        return direccion;
    }
    
    /**
     * enivar direccion <br>
     * @param direccion String
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * 
     * @return String - devulve el numero de telefono del usuario
     */
    public String getTlf() {
        return tlf;
    }

    /**
     * 
     * @param tlf numero de telefono
     */
    public void setTlf(String tlf) {
        this.tlf = tlf;
    }

    /**
     * 
     * @return String - devuelve el email del usuario
     */
    public String getEmail() {
        return email;
    }

    /**
     * enivar dato dfe email<br>
     * @param email String
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return String - devulve la contraseña del usuario
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param password contraseña
     */
    public void setPassword(String password) {
        this.password = password;
    }    
    
}