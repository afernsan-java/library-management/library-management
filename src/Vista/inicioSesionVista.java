package Vista;

import Negocio.AdminClass;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Abraham Fernandez Sanchez 
 */
public class inicioSesionVista extends javax.swing.JInternalFrame {
    Escritorio escritorio ; 
    JFrame frame;
    AdminClass admin;

    /**
     * Creates new form inicioSesionVentana
     * @param escritorio Objeto de la clase Escritorio
     */
    public inicioSesionVista(Escritorio escritorio) {         
        this.escritorio=escritorio;           
        initComponents();        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButtonAceptar = new javax.swing.JButton();
        jButtonReset = new javax.swing.JButton();
        jTextFieldNameUsuario = new javax.swing.JTextField();
        jPasswordUser = new javax.swing.JPasswordField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Inicio Sesion");

        jLabel1.setText("Nombre:");

        jLabel2.setText("Contraseña:");

        jButtonAceptar.setText("Aceptar");
        jButtonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAceptarActionPerformed(evt);
            }
        });

        jButtonReset.setText("Reset");
        jButtonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetActionPerformed(evt);
            }
        });

        jTextFieldNameUsuario.setToolTipText("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextFieldNameUsuario)
                            .addComponent(jPasswordUser, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonAceptar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButtonReset)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldNameUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jPasswordUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonAceptar)
                    .addComponent(jButtonReset))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetActionPerformed
        jTextFieldNameUsuario.setText("");
        jPasswordUser.setText("");        
    }//GEN-LAST:event_jButtonResetActionPerformed

    private void jButtonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAceptarActionPerformed
        try {
                admin = new AdminClass();
                String nomAdmin = jTextFieldNameUsuario.getText();
                String pass= String.valueOf(jPasswordUser.getPassword());      

                if(admin.loginAdmin(nomAdmin, pass)){
                    JOptionPane.showMessageDialog(escritorio, "Los datos introducidos son correctos");               
                    this.setClosed(true);//metodo para cerrar este frame                
                        try{                
                        escritorio.menusOn();
                        admin.setAdminNow(nomAdmin);
                        admin.setPasswordNow(pass);
                        admin.setAdminLogeado(true);                      
                        }catch(NullPointerException es){
                             System.out.println("Salto una excepcion NullPointerException");
                         }
                 }else{                
                        try{
                            JOptionPane.showMessageDialog(escritorio, "Los datos introducidos NO son correctos");
                           admin.setAdminLogeado(false);     
                        }catch(NullPointerException es){
                            System.out.println("Salto una excepcion NullPointerException");
                        }
                 }  
        } catch (SQLException | PropertyVetoException ex) {
            Logger.getLogger(inicioSesionVista.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }//GEN-LAST:event_jButtonAceptarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAceptar;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPasswordField jPasswordUser;
    private javax.swing.JTextField jTextFieldNameUsuario;
    // End of variables declaration//GEN-END:variables

}
