package Vista;

import DAO.ClaseBBDD;
import Negocio.AdminClass;
import Negocio.BookClass;
import Negocio.UserClass;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Abraham Fernandez Sanchez 
 */

public final class Escritorio extends javax.swing.JFrame {
    
     AdminClass admin ;
     BookClass libro;
     static UserClass userClass;
//     ClaseBBDD BBDD = new ClaseBBDD();
    
    final String VISTA_INICIO_SESION ="inicioSesion";
    final String VISTA_CAMBIO_PASSWORD ="cambioPassword";
    final String VISTA_ADD_LIBRO="addLibro";
    final String VISTA_ADD_USER="addUser";
    final String VISTA_CONSULTAR_LIBRO="consultarLibro";
    final String VISTA_DEVOLUCION_LIBRO="devolucionLibro";
    final String VISTA_ELIMINAR_LIBRO="eliminarLibro";
    final String VISTA_ELIMINAR_USUARIO="eliminarUser";
    final String VISTA_HIST_PREST="historialPrestamos";
    final String VISTA_MODIFICAR_LIBRO="modificarLibro";
    final String VISTA_MODIFICAR_USUARIO="modificarUsuario";
    final String VISTA_PRESTAMO_ACTUAL="prestamoActual";
    final String VISTA_PRESTAMO_LIBRO="prestamoLibro";    
    
    /**
     * Creates new form Escritorio
     * @throws java.sql.SQLException Excepcion de tipo SQL
     * @throws java.text.ParseException Excepcion de tipo parse
     */
    public Escritorio() throws SQLException, ParseException {  
  
        arrancarAplicacion();
//        BBDD.listadoPrestamosBookActuales();
        
    }     
    

    public void arrancarAplicacion(){
        admin = new AdminClass();
      
        admin.instalacionBBDD();

        initComponents(); 
        menusOff();//menus de administrador desactivados 
    
    }
    
    
    public void nuevaVentanaInterior(String nombreVentana) throws PropertyVetoException{
                  
          //SWICTH CON TODAS LAS OPCIONES DEL MENU Y SEGUN EL MENU QUE SE CLICKE CREARA
          //UNA VENTANA U OTRA
    
        switch(nombreVentana){
            case VISTA_INICIO_SESION:               
                inicioSesionVista vistaIniSes = new inicioSesionVista(this);                     
                    jDesktopPane1.add(vistaIniSes);
                    vistaIniSes.setVisible(true);                                  
                break; 
           
            case VISTA_CAMBIO_PASSWORD:                        
                    CambioAdminPasswordVista vCambContra = new CambioAdminPasswordVista(this);    
                    jDesktopPane1.add(vCambContra);                 
                        vCambContra.setVisible(true);
                break;
           
            case VISTA_ADD_LIBRO:
                 AddLibroVista vAddLibro = new AddLibroVista(this);    
                    jDesktopPane1.add(vAddLibro);
                    vAddLibro.setVisible(true); 
                break;

             case VISTA_ADD_USER:
                 AddUsuarioVista vAddUser;    
                    try {
                        vAddUser = new AddUsuarioVista(this);
                          jDesktopPane1.add(vAddUser);
                          vAddUser.setVisible(true); 
                    } catch (SQLException ex) {
                        Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
                    }                  
                break;   

             case VISTA_CONSULTAR_LIBRO:
                  ConsultarLibroVista vConLib = new ConsultarLibroVista(this);    
                    jDesktopPane1.add(vConLib);
                    vConLib.setVisible(true); 
                break;

             case VISTA_DEVOLUCION_LIBRO:
                   DevolucionLibroVista vDevLib = new DevolucionLibroVista(this);    
                    jDesktopPane1.add(vDevLib);
                    vDevLib.setVisible(true); 
                break;

             case VISTA_ELIMINAR_USUARIO:
                  EliminarUserVista vElimUser = new EliminarUserVista(this);    
                    jDesktopPane1.add(vElimUser);
                    vElimUser.setVisible(true);                  
                break;

             case VISTA_ELIMINAR_LIBRO:
                 EliminarLibroVista vElimLib = new EliminarLibroVista(this);    
                    jDesktopPane1.add(vElimLib);
                    vElimLib.setVisible(true); 
                break;    

             case VISTA_HIST_PREST:
                  HistorialPrestamosVista vHistPres = new HistorialPrestamosVista(this);    
                    jDesktopPane1.add(vHistPres);
                    vHistPres.setVisible(true); 
                break;

             case VISTA_MODIFICAR_LIBRO:
                 ModificarLibroVista vModLib = new ModificarLibroVista(this);
                    jDesktopPane1.add(vModLib);
                    vModLib.setVisible(true); 
                break;

             case VISTA_MODIFICAR_USUARIO:
                 ModificarUsuarioVista vModUser = new ModificarUsuarioVista(this);    
                    jDesktopPane1.add(vModUser);
                    vModUser.setVisible(true);              
                break;

             case VISTA_PRESTAMO_ACTUAL:
                  PrestamoActualVista vPresAct = new PrestamoActualVista(this);    
                    jDesktopPane1.add(vPresAct);
                    vPresAct.setVisible(true);  
                break;  

             case VISTA_PRESTAMO_LIBRO:
                 PrestamoNewBookVista vPresLibro = new PrestamoNewBookVista(this);    
                    jDesktopPane1.add(vPresLibro);
                    vPresLibro.setVisible(true);  
                break;

             default:                

             break;

        }
    
    }    

    //activa el visionado de los menus cuando el logeo del admin exista y sea correcto
    public void menusOn(){   
            
              jMenuUsuariuos.setVisible(true);
              jMenuLibros.setVisible(true);
              jMenuPrestamos.setVisible(true);
              jMenuItemCambioPassword.setVisible(true);
              jMenuItemSalirSesion.setVisible(true);
              jMenuItemIniciarSesion.setVisible(false);
    }
    
    //desactiva el visionado de los menus cuando el logeo del admin no exista
    public void menusOff(){   
             jMenuItemIniciarSesion.setVisible(true);
             jMenuUsuariuos.setVisible(false);
             jMenuLibros.setVisible(false);
             jMenuPrestamos.setVisible(false);
             jMenuItemCambioPassword.setVisible(false);
             jMenuItemSalirSesion.setVisible(false);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuIniciarSesion = new javax.swing.JMenu();
        jMenuItemIniciarSesion = new javax.swing.JMenuItem();
        jMenuItemCambioPassword = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItemSalirSesion = new javax.swing.JMenuItem();
        jMenuItemSalirPrograma = new javax.swing.JMenuItem();
        jMenuUsuariuos = new javax.swing.JMenu();
        jMenuItemAddUser = new javax.swing.JMenuItem();
        jMenuItemElimnarUser = new javax.swing.JMenuItem();
        jMenuItemEditarUser = new javax.swing.JMenuItem();
        jMenuLibros = new javax.swing.JMenu();
        jMenuItemAddLibro = new javax.swing.JMenuItem();
        jMenuItemModiLibro = new javax.swing.JMenuItem();
        jMenuItemConsulLibro = new javax.swing.JMenuItem();
        jMenuItemEliminarLibro = new javax.swing.JMenuItem();
        jMenuPrestamos = new javax.swing.JMenu();
        jMenuItemPrestamoLibro = new javax.swing.JMenuItem();
        jMenuItemDevolucionLibro = new javax.swing.JMenuItem();
        jMenuItemHistPrestamos = new javax.swing.JMenuItem();
        jMenuItemPrestamoActual = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Biblioteca");
        setExtendedState(6);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 479, Short.MAX_VALUE)
        );

        jMenuIniciarSesion.setText("Sesion");

        jMenuItemIniciarSesion.setText("Iniciar Sesion");
        jMenuItemIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemIniciarSesionActionPerformed(evt);
            }
        });
        jMenuIniciarSesion.add(jMenuItemIniciarSesion);

        jMenuItemCambioPassword.setText("Cambiar contraseña");
        jMenuItemCambioPassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemCambioPasswordActionPerformed(evt);
            }
        });
        jMenuIniciarSesion.add(jMenuItemCambioPassword);
        jMenuIniciarSesion.add(jSeparator1);

        jMenuItemSalirSesion.setText("Salir Sesion");
        jMenuItemSalirSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSalirSesionActionPerformed(evt);
            }
        });
        jMenuIniciarSesion.add(jMenuItemSalirSesion);

        jMenuItemSalirPrograma.setText("Salir");
        jMenuItemSalirPrograma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSalirProgramaActionPerformed(evt);
            }
        });
        jMenuIniciarSesion.add(jMenuItemSalirPrograma);

        jMenuBar1.add(jMenuIniciarSesion);

        jMenuUsuariuos.setText("Usuarios");

        jMenuItemAddUser.setText("Añadir Ususario");
        jMenuItemAddUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAddUserActionPerformed(evt);
            }
        });
        jMenuUsuariuos.add(jMenuItemAddUser);

        jMenuItemElimnarUser.setText("Eliminar Usuario");
        jMenuItemElimnarUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemElimnarUserActionPerformed(evt);
            }
        });
        jMenuUsuariuos.add(jMenuItemElimnarUser);

        jMenuItemEditarUser.setText("Modificar Usuario");
        jMenuItemEditarUser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEditarUserActionPerformed(evt);
            }
        });
        jMenuUsuariuos.add(jMenuItemEditarUser);

        jMenuBar1.add(jMenuUsuariuos);

        jMenuLibros.setText("Libros");

        jMenuItemAddLibro.setText("Añadir Libro");
        jMenuItemAddLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemAddLibroActionPerformed(evt);
            }
        });
        jMenuLibros.add(jMenuItemAddLibro);

        jMenuItemModiLibro.setText("Modificar Libro");
        jMenuItemModiLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemModiLibroActionPerformed(evt);
            }
        });
        jMenuLibros.add(jMenuItemModiLibro);

        jMenuItemConsulLibro.setText("Consultar Libro");
        jMenuItemConsulLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemConsulLibroActionPerformed(evt);
            }
        });
        jMenuLibros.add(jMenuItemConsulLibro);

        jMenuItemEliminarLibro.setText("Eliminar Libro");
        jMenuItemEliminarLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemEliminarLibroActionPerformed(evt);
            }
        });
        jMenuLibros.add(jMenuItemEliminarLibro);

        jMenuBar1.add(jMenuLibros);

        jMenuPrestamos.setText("Prestamos");

        jMenuItemPrestamoLibro.setText("Prestamo Libro");
        jMenuItemPrestamoLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPrestamoLibroActionPerformed(evt);
            }
        });
        jMenuPrestamos.add(jMenuItemPrestamoLibro);

        jMenuItemDevolucionLibro.setText("Devolucion Libro");
        jMenuItemDevolucionLibro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemDevolucionLibroActionPerformed(evt);
            }
        });
        jMenuPrestamos.add(jMenuItemDevolucionLibro);

        jMenuItemHistPrestamos.setText("Historial Prestamos");
        jMenuItemHistPrestamos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemHistPrestamosActionPerformed(evt);
            }
        });
        jMenuPrestamos.add(jMenuItemHistPrestamos);

        jMenuItemPrestamoActual.setText("Prestamo Actual");
        jMenuItemPrestamoActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPrestamoActualActionPerformed(evt);
            }
        });
        jMenuPrestamos.add(jMenuItemPrestamoActual);

        jMenuBar1.add(jMenuPrestamos);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemIniciarSesionActionPerformed
        try {
            
            nuevaVentanaInterior(VISTA_INICIO_SESION);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }//GEN-LAST:event_jMenuItemIniciarSesionActionPerformed

    private void jMenuItemSalirProgramaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSalirProgramaActionPerformed
         System.exit(0);//salir de esta ventana
    }//GEN-LAST:event_jMenuItemSalirProgramaActionPerformed

    private void jMenuItemCambioPasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemCambioPasswordActionPerformed
                 
        try {
            nuevaVentanaInterior(VISTA_CAMBIO_PASSWORD);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jMenuItemCambioPasswordActionPerformed

    private void jMenuItemAddUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAddUserActionPerformed
        try {
            nuevaVentanaInterior(VISTA_ADD_USER);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemAddUserActionPerformed

    private void jMenuItemElimnarUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemElimnarUserActionPerformed
         try {
            nuevaVentanaInterior(VISTA_ELIMINAR_USUARIO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }//GEN-LAST:event_jMenuItemElimnarUserActionPerformed

    private void jMenuItemEditarUserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEditarUserActionPerformed
        try {
            nuevaVentanaInterior(VISTA_MODIFICAR_USUARIO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemEditarUserActionPerformed

    private void jMenuItemAddLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemAddLibroActionPerformed
         try {
            nuevaVentanaInterior(VISTA_ADD_LIBRO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemAddLibroActionPerformed

    private void jMenuItemModiLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemModiLibroActionPerformed
         try {
            nuevaVentanaInterior(VISTA_MODIFICAR_LIBRO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemModiLibroActionPerformed

    private void jMenuItemConsulLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemConsulLibroActionPerformed
         try {
            nuevaVentanaInterior(VISTA_CONSULTAR_LIBRO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemConsulLibroActionPerformed

    private void jMenuItemEliminarLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemEliminarLibroActionPerformed
        try {
            nuevaVentanaInterior(VISTA_ELIMINAR_LIBRO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemEliminarLibroActionPerformed

    private void jMenuItemPrestamoLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPrestamoLibroActionPerformed
       try {
            nuevaVentanaInterior(VISTA_PRESTAMO_LIBRO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemPrestamoLibroActionPerformed

    private void jMenuItemDevolucionLibroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemDevolucionLibroActionPerformed
        try {
            nuevaVentanaInterior(VISTA_DEVOLUCION_LIBRO);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemDevolucionLibroActionPerformed

    private void jMenuItemHistPrestamosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemHistPrestamosActionPerformed
       try {
            nuevaVentanaInterior(VISTA_HIST_PREST);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemHistPrestamosActionPerformed

    private void jMenuItemPrestamoActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPrestamoActualActionPerformed
      try {
            nuevaVentanaInterior(VISTA_PRESTAMO_ACTUAL);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItemPrestamoActualActionPerformed

    private void jMenuItemSalirSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSalirSesionActionPerformed
     
        admin = new AdminClass();
        admin.setAdminLogeado(false);
        menusOff();
        
    }//GEN-LAST:event_jMenuItemSalirSesionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Escritorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Escritorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Escritorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Escritorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Escritorio().setVisible(true);
                } catch (SQLException | ParseException ex) {
                    Logger.getLogger(Escritorio.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenu jMenuIniciarSesion;
    private javax.swing.JMenuItem jMenuItemAddLibro;
    private javax.swing.JMenuItem jMenuItemAddUser;
    private javax.swing.JMenuItem jMenuItemCambioPassword;
    private javax.swing.JMenuItem jMenuItemConsulLibro;
    private javax.swing.JMenuItem jMenuItemDevolucionLibro;
    private javax.swing.JMenuItem jMenuItemEditarUser;
    private javax.swing.JMenuItem jMenuItemEliminarLibro;
    private javax.swing.JMenuItem jMenuItemElimnarUser;
    private javax.swing.JMenuItem jMenuItemHistPrestamos;
    private javax.swing.JMenuItem jMenuItemIniciarSesion;
    private javax.swing.JMenuItem jMenuItemModiLibro;
    private javax.swing.JMenuItem jMenuItemPrestamoActual;
    private javax.swing.JMenuItem jMenuItemPrestamoLibro;
    private javax.swing.JMenuItem jMenuItemSalirPrograma;
    private javax.swing.JMenuItem jMenuItemSalirSesion;
    private javax.swing.JMenu jMenuLibros;
    private javax.swing.JMenu jMenuPrestamos;
    private javax.swing.JMenu jMenuUsuariuos;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    // End of variables declaration//GEN-END:variables


}
