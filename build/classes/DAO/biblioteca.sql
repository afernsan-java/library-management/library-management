-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-04-2018 a las 18:24:15
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `biblioteca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `nombre`, `password`) VALUES
(1, 'abraham', '1111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro`
--

CREATE TABLE `libro` (
  `id` int(11) NOT NULL,
  `isbn` int(20) NOT NULL,
  `titulo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_520_ci NOT NULL,
  `editorial` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_520_ci NOT NULL,
  `carnet_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `libro`
--

INSERT INTO `libro` (`id`, `isbn`, `titulo`, `editorial`, `carnet_usuario`) VALUES
(1, 345047656, '  Argentina, Legend and History ', 'Morbi Quis Industries', NULL),
(2, 278652917, '  Catálogo Monumental de España; Provincia de Álava ', 'Nonummy Ut LLP', NULL),
(3, 790526971, '  Cuentos de mi tiempo ', 'Iaculis Odio Nam Consulting', NULL),
(4, 986388808, '  Don Francisco de Quevedo: Drama en Cuatro Actos ', 'Cras Corp.', NULL),
(5, 638601692, '  Bailén ', 'Proin Industries', NULL),
(6, 687295211, '  Diario de la expedicion de 1822 a los campos del sur de Buenos Aires ', 'am Scelerisque Company', NULL),
(7, 916697045, '  Algo de todo ', 'Tincidunt Associates', NULL),
(8, 861287660, '  Doña Clarines y Mañana de Sol ', 'Rutrum Urna Nec LLC', NULL),
(9, 9554954, '  Carlos Broschi ', 'At Company', NULL),
(10, 465860193, '  Diario de un viage a la costa de la mar Magallanica ', 'Imperdiet Associates', NULL),
(11, 993692173, '  Clásicos Castellanos: Libro de Buen Amor ', 'Ligula am PC', NULL),
(12, 65654911, '  El Diablo Cojuelo ', 'Nibh Phasellus Inc.', NULL),
(13, 669645239, '  Cuentos de Amor de Locura y de Muerte ', 'Mi Felis Institute', NULL),
(14, 315169563, '  De Las Islas Filipinas ', 'Sit Amet Ultricies Incorporated', NULL),
(15, 941646577, '  Ariel ', 'Orci Industries', NULL),
(16, 360387209, '  El derecho internacional americano ', 'Venenatis A LLP', NULL),
(17, 838353134, '  Amaury ', 'Ac Urna Inc.', NULL),
(18, 590769696, '  Cuentos de mi tiempo ', 'Enim Corporation', NULL),
(19, 376056175, '  Cocina del tiempo, ó arte de preparar sabrosos y exquisitos platos propios de cada estación ', 'Vel Arcu Eu Industries', NULL),
(20, 26659155, '40 años de proyecto Gútenberg ', 'Mauris Integer Sem Company', NULL),
(21, 436019736, '  De Las Islas Filipinas ', 'Dictum Foundation', NULL),
(22, 11525384, '  Comedias: El remedio en la desdicha; El mejor alcalde, el rey ', 'Luctus Limited', NULL),
(23, 618299909, '  Algo de todo ', 'Mi Ac Mattis Corp.', NULL),
(24, 912195095, '  Cuentos de Amor de Locura y de Muerte ', 'Malesuada Consulting', NULL),
(25, 65612827, '  Catálogo Instructivo de las Colecciones Mineralógicas ', 'Lacus Corporation', NULL),
(26, 224464996, '  Bocetos californianos ', 'Imperdiet Ornare Associates', NULL),
(27, 960106817, '  Cádiz ', 'Nisi Nibh Industries', NULL),
(28, 824051701, '  A First Spanish Reader ', 'Rutrum LLC', NULL),
(29, 552420984, '  Amaury ', 'Blandit Viverra Donec Industries', NULL),
(30, 331285868, '  Del libro impreso al libro digital ', 'Felis Limited', NULL),
(31, 492173809, '  Diario de la navegacion emprendida en 1781 ', 'Duis Sit Ltd', NULL),
(32, 581620380, '  Consideraciones Sobre el Origen del Nombre de los Números en Tagalog ', 'Fusce Dolor Company', NULL),
(33, 928277064, '  Doctrina Christiana ', 'Blandit Enim Consequat Foundation', NULL),
(34, 87087249, '  Cocina del tiempo, ó arte de preparar sabrosos y exquisitos platos propios de cada estación ', 'Non Institute', NULL),
(35, 486598757, '  El cocinero de su majestad ', 'a Limited', NULL),
(36, 737779872, '  Derroteros y viages à la Ciudad Encantada, ó de los Césares. ', 'Ullamcorper Nisl Incorporated', NULL),
(37, 245579672, '  El Estudiante de Salamanca and Other Selections ', 'Integer Vulputate Risus Ltd', NULL),
(38, 769814884, '  Aguas fuertes ', 'A Neque am PC', NULL),
(39, 461823262, '  Al primer vuelo ', 'Ornare Lectus Inc.', NULL),
(40, 347730822, '  Del libro impreso al libro digital ', 'Praesent Foundation', NULL),
(41, 8134981, '  Bocetos californianos ', 'Mauris Vel Incorporated', NULL),
(42, 298202710, '40 años de proyecto Gútenberg ', 'Dictum Proin Limited', NULL),
(43, 236527266, '  El Proyecto Gutenberg (1971-2009) ', 'Lorem Semper Institute', NULL),
(44, 350318485, '  Cuentos de Amor de Locura y de Muerte ', 'Odio Vel Est Associates', NULL),
(45, 253329173, '  El Manuscrito de mi madre ', 'Non Inc.', NULL),
(46, 594378824, '  Biografia de Simon Bolívar ', 'Pellentesque Sed Dictum Limited', NULL),
(47, 684053909, '  Cocina cómica ', 'Auctor Vitae Corp.', NULL),
(48, 583150645, '  El Mandarín ', 'In Incorporated', NULL),
(49, 381414172, '  Diccionario Bagobo-Español ', 'Magna Sed Institute', NULL),
(50, 25598225, '  Biografia de Simon Bolívar ', 'Ipsum Non Arcu Industries', NULL),
(51, 546739885, '  Diario de la navegacion emprendida en 1781 ', 'Sed LLP', NULL),
(52, 383936329, '  El Filibusterismo (Continuación del Noli me tángere) ', 'Ante Vivamus Foundation', NULL),
(53, 147136645, '  Curiosidades antiguas sevillanas ', 'Adipiscing Ltd', NULL),
(54, 631208684, '  Curiosidades antiguas sevillanas ', 'Sed Facilisis Industries', NULL),
(55, 283572561, '  Adriana Zumarán ', 'Egestas Institute', NULL),
(56, 825887747, '  Diario de la navegacion emprendida en 1781 ', 'Pulvinar Arcu Et Inc.', NULL),
(57, 529489575, '  Descripción colonial, libro segundo (2/2) ', 'Suspendisse Limited', NULL),
(58, 505416947, '  El Estudiante de Salamanca and Other Selections ', 'Nec Institute', NULL),
(59, 431035304, '  Actas capitulares desde el 21 - 25 de mayo de 1810 en Buenos Aires ', 'At Libero Morbi Limited', NULL),
(60, 632680167, '  Correspondencia Oficial sobre la Demarcacion de Limites entre el Paraguay y el Brasil ', 'Porttitor Vulputate Limited', NULL),
(61, 772001052, '  Cuentos de mi tiempo ', 'Id Limited', NULL),
(62, 565267521, '  Diario de la navegacion emprendida en 1781 ', 'Consequat PC', NULL),
(63, 701791314, '  Catálogo de los Objetos Etnologicos y Arqueologicos Exhibidos por la Expedición Hemenway ', 'Et Nunc Quisque Corporation', NULL),
(64, 688318658, '  Carlos Broschi ', 'Nec Corporation', NULL),
(65, 500031892, '  El Capitán Veneno ', 'Vitae Sodales At Ltd', NULL),
(66, 496624101, '  Diccionario Bagobo-Español ', 'Sit Amet Industries', NULL),
(67, 46224201, '  El Mar ', 'Condimentum Eget Volutpat Industries', NULL),
(68, 395152509, '  El Abate Constantin ', 'Porttitor Tellus LLP', NULL),
(69, 736440331, '  Bocetos californianos ', 'Adipiscing Inc.', NULL),
(70, 322726734, '  Biografia de Simon Bolívar ', 'Vivamus Nisi Industries', NULL),
(71, 107381097, '  A vuela pluma ', 'Non Sapien PC', NULL),
(72, 709534563, '  Cuentos de Amor de Locura y de Muerte ', 'Sed Corp.', NULL),
(73, 585245206, '  Colección de viages y expediciónes à los campos de Buenos Aires y a las costas de Patagonia ', 'Ornare Facilisis Eget Corporation', NULL),
(74, 687392583, '  Cádiz ', 'Metus Vivamus Inc.', NULL),
(75, 97891472, '  El Jayón ', 'Sed Dolor Inc.', NULL),
(76, 834471844, '  Diario del viaje al rio Bermejo ', 'Congue In Scelerisque Limited', NULL),
(77, 28832471, '  Actas capitulares desde el 21 - 25 de mayo de 1810 en Buenos Aires ', 'Ut a Cras Inc.', NULL),
(78, 608968062, '  Cuentos de mi tiempo ', 'Lorem Foundation', NULL),
(79, 416264166, '  Doña Luz ', 'Lectus Corp.', NULL),
(80, 419400214, '  Cocina cómica ', 'Id Mollis Nec Foundation', NULL),
(81, 191743611, '  El cuarto poder ', 'Enim Ltd', NULL),
(82, 499382205, '  El cocinero de su majestad ', 'Pede Ac Associates', NULL),
(83, 620420949, '  El Mar ', 'Nisl LLC', NULL),
(84, 59705481, '  Descripción Geografica, Histórica y Estadística de Bolivia, Tomo 1. ', 'Iaculis Lacus Pede Company', NULL),
(85, 532326375, '  Angelina ', 'Sem Vitae Corp.', NULL),
(86, 260530530, '  Don Quijote ', 'Auctor Company', NULL),
(87, 362348970, '  Diario de un reconocimiento de la guardia y fortines ', 'Quis Tristique Ac LLC', NULL),
(88, 552410212, '  Adriana Zumarán ', 'Mauris Inc.', NULL),
(89, 750440999, '  El Mandarín ', 'Scelerisque Dui LLP', NULL),
(90, 312214648, '  Dulce y sabrosa ', 'Eget Limited', NULL),
(91, 878482715, '  El Proyecto Gutenberg (1971-2009) ', 'A PC', NULL),
(92, 571126883, '  A vuela pluma ', 'Morbi Tristique Senectus Corporation', NULL),
(93, 129558179, '  Catálogo de los Objetos Etnologicos y Arqueologicos Exhibidos por la Expedición Hemenway ', 'Sociis Natoque Penatibus Corporation', NULL),
(94, 252085857, '  Booknología: El libro digital (1971-2010) ', 'Porttitor Eros Nec PC', NULL),
(95, 576564616, '  Diccionario Ingles-Español-Tagalog ', 'Etiam Imperdiet Dictum Institute', NULL),
(96, 8304961, '  Diario de un viage a la costa de la mar Magallanica ', 'Ac Eleifend Foundation', NULL),
(97, 232075676, '  El cuarto poder ', 'Orci In Corp.', NULL),
(98, 81905180, '  Antonio Azorín ', 'Dictum Limited', NULL),
(99, 915823211, '  Bailén ', 'Aliquam Rutrum Lorem Ltd', NULL),
(100, 541507759, '  El Gaucho Martín Fierro ', 'Lectus Pede Et Corporation', NULL),
(105, 1234567, 'pru4eba', 'preuba56', 0),
(106, 7654321, 'titulo de prueba', 'editorial de prueba', 1234567);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamos_libros`
--

CREATE TABLE `prestamos_libros` (
  `id` int(11) NOT NULL,
  `isbn` int(20) NOT NULL,
  `carnet_usuario` int(11) NOT NULL,
  `f_salida` date DEFAULT NULL,
  `f_devolucion` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `prestamos_libros`
--

INSERT INTO `prestamos_libros` (`id`, `isbn`, `carnet_usuario`, `f_salida`, `f_devolucion`) VALUES
(4, 1234567, 1234567, '2018-04-01', NULL),
(3, 7654321, 1234567, '2018-04-01', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `carnet` int(11) NOT NULL,
  `nombre` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_520_ci NOT NULL,
  `apellidos` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_520_ci NOT NULL,
  `direccion` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_520_ci NOT NULL,
  `telefono` varchar(12) CHARACTER SET utf8 COLLATE utf8_unicode_520_ci NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `carnet`, `nombre`, `apellidos`, `direccion`, `telefono`, `email`, `password`) VALUES
(11, 10000, 'Perry', 'Benton', 'Apartado núm.: 938, 5724 In C.', '034641365861', 'pede@idmagna.com', 'VIO76SQG4TQ'),
(12, 10001, 'Nicholas', 'Johnson', 'Apdo.:402-5743 Orci C.', '034364467576', 'feugiat@liberoat.edu', 'DCC77YGH3RZ'),
(13, 10002, 'Dieter', 'Hardy', 'Apartado núm.: 308, 9318 Justo Avda.', '034560549661', 'risus.Morbi@ipsumleoelementum.ca', 'CBR34VED7ZQ'),
(14, 10003, 'Azalia', 'Santiago', '263-2723 Ut Carretera', '034976869489', 'Praesent.eu@erat.ca', 'IIT27WDS6YL'),
(15, 10004, 'Mason', 'Underwood', '9389 Tempus ', '034599956644', 'malesuada@laoreet.ca', 'CPT54CSK9VX'),
(16, 10005, 'Wynter', 'Mcmahon', 'Apdo.:447-6943 Aliquet, Ctra.', '034184684662', 'mi@feugiat.edu', 'NLG95UMM9GY'),
(17, 10006, 'Ian', 'Kinney', 'Apartado núm.: 739, 6754 At, C/', '034658103507', 'Aliquam.auctor@nec.net', 'VEJ03VLS8GX'),
(18, 10007, 'Russell', 'Richards', '145-717 Magna. Calle', '034428913773', 'enim.sit@natoque.net', 'FOR20ERW9KQ'),
(19, 10008, 'Hector', 'Boone', 'Apdo.:333-7466 Consectetuer C/', '034644949799', 'Cum.sociis.natoque@QuisquevariusNam.edu', 'WZA52WLA2FO'),
(20, 10009, 'Alexa', 'Bennett', '464-5351 Nec, C.', '034750294647', 'gravida.non.sollicitudin@ametconsectetuer.co.uk', 'GAK51JBD1IW'),
(21, 10010, 'Gregory', 'Sanders', '5498 Cursus C.', '034264511182', 'eu@ridiculusmus.com', 'USA12EEZ6HE'),
(22, 10011, 'Jonas', 'Mayer', 'Apdo.:934-7995 Eros. Avenida', '034837541398', 'tempus@aclibero.ca', 'XMH25RRQ0ZF'),
(23, 10012, 'Lacota', 'Castro', '627-9699 Aliquet ', '034740778328', 'non.lorem.vitae@malesuadaaugueut.edu', 'QJO93KOH1MN'),
(24, 10013, 'Baxter', 'Downs', '4390 Sit Carretera', '034832578610', 'ac@nonhendrerit.net', 'MEL89UOU8IB'),
(25, 10014, 'Ferriss', 'Barry', '8879 Quisque Avenida', '034978543608', 'risus.a.ultricies@Sedpharetra.edu', 'YFB08RUG4MV'),
(26, 10015, 'Brett', 'Greene', 'Apdo.:265-1069 Dapibus ', '034478955101', 'Sed.pharetra.felis@liberoProinsed.org', 'OWV61IPC1YZ'),
(27, 10016, 'Lane', 'Gordon', 'Apartado núm.: 856, 4920 Et, Avda.', '034209125220', 'neque.pellentesque.massa@ullamcorper.net', 'TAX23NSC9RM'),
(28, 10017, 'Declan', 'Mccray', 'Apartado núm.: 779, 1458 Diam. ', '034906206290', 'fames@idliberoDonec.net', 'POS77NJY9IY'),
(29, 10018, 'Leilani', 'Nolan', 'Apartado núm.: 515, 3699 Dictum C.', '034918862257', 'imperdiet.erat.nonummy@a.edu', 'LAM61QWO5FR'),
(30, 10019, 'Reece', 'Bauer', 'Apdo.:584-4308 Nibh. Ctra.', '034607502344', 'vestibulum.neque.sed@Quisqueliberolacus.edu', 'LHR07HSM7FW'),
(31, 10020, 'McKenzie', 'Mclean', '765-5864 Nisi Av.', '034477577668', 'vulputate.risus@Aliquam.com', 'EOD75OFR5ES'),
(32, 10021, 'Christopher', 'Benton', 'Apartado núm.: 723, 8470 Sociis C.', '034645126324', 'natoque.penatibus@nonlaciniaat.co.uk', 'CCB13RJO2SX'),
(33, 10022, 'Nell', 'Vinson', 'Apartado núm.: 100, 3149 Id, Avda.', '034136604403', 'consequat.purus@fermentumvel.ca', 'RXP12OAX3UU'),
(34, 10023, 'Barrett', 'Obrien', 'Apdo.:686-1003 Augue Calle', '034874145370', 'Sed.eu.eros@dignissimmagnaa.org', 'LDZ98DDE1FW'),
(35, 10024, 'Yuri', 'Puckett', 'Apdo.:372-7470 Luctus Av.', '034827946149', 'vel.venenatis.vel@nisi.net', 'ZXV60RHF5GN'),
(36, 10025, 'Rose', 'Wong', 'Apartado núm.: 380, 8425 Lobortis. Av.', '034708380924', 'neque.Nullam@sit.edu', 'SSR15BAC8DL'),
(37, 10026, 'Pandora', 'Crawford', '3787 Id, Calle', '034428949169', 'congue.In@natoquepenatibus.edu', 'JDP81XLP2YZ'),
(38, 10027, 'Azalia', 'Cantu', '125-2571 Nisi Ctra.', '034961540270', 'vestibulum.neque.sed@conubia.edu', 'ZJA16EEN9HQ'),
(39, 10028, 'Harlan', 'Stevens', '223-6474 Adipiscing. C.', '034728336647', 'nec.malesuada.ut@nonummyipsumnon.ca', 'UXC73XJY1AU'),
(40, 10029, 'Indigo', 'Orr', 'Apdo.:649-945 Lorem, C.', '034634980638', 'vitae.nibh@lacus.com', 'FVA51YIE3QS'),
(41, 10030, 'Quentin', 'Knapp', 'Apartado núm.: 837, 7725 Ridiculus Avda.', '034575415503', 'Sed.molestie.Sed@Phasellusornare.edu', 'JIF32EMZ3AD'),
(42, 10031, 'Giacomo', 'Price', '3507 Sociis Calle', '034331381270', 'blandit.Nam.nulla@iaculis.ca', 'XTB37PLN0PC'),
(43, 10032, 'Dale', 'Burke', '895 Ullamcorper. ', '034881155912', 'orci@cursusin.ca', 'ZEE97GPL5GL'),
(44, 10033, 'Emmanuel', 'Barlow', '3497 Eleifend, Avenida', '034268613319', 'vestibulum.Mauris.magna@acfermentum.net', 'GJU82CAJ7JU'),
(45, 10034, 'Bert', 'Howard', '9838 Dui C.', '034976531291', 'egestas.Duis@semper.org', 'CFC57TOV9LZ'),
(46, 10035, 'Cameron', 'Randolph', '443-858 Est Carretera', '034325781923', 'non@tincidunt.ca', 'ZSV47GDA0ZU'),
(47, 10036, 'Nina', 'Wells', 'Apdo.:140-6917 Est. ', '034438905697', 'nisi.nibh@portaelita.edu', 'CYS03MLZ1ZB'),
(48, 10037, 'Victor', 'Rasmussen', '246-4948 Ut Avda.', '034905964417', 'malesuada@nuncullamcorpereu.com', 'ICG18LKL5IQ'),
(49, 10038, 'Elton', 'Molina', '287-8021 Aliquet. Avda.', '034636855203', 'ipsum.Donec.sollicitudin@Cumsociis.com', 'HJU96BNO0IS'),
(50, 10039, 'Sasha', 'Montgomery', 'Apartado núm.: 211, 3652 Nulla Calle', '034934444586', 'Nulla@arcuCurabiturut.ca', 'CIX45OHA0SN'),
(51, 10040, 'Amos', 'Stein', '8057 Malesuada Avenida', '034770657747', 'Sed@justofaucibus.org', 'CEK57OYR8CD'),
(52, 10041, 'Acton', 'Sandoval', 'Apartado núm.: 390, 5312 Pede. Avenida', '034769615983', 'vestibulum.massa@fringillaDonecfeugiat.com', 'DYE60FQJ2XZ'),
(53, 10042, 'Fiona', 'Tucker', '2183 Sollicitudin Carretera', '034438741549', 'eget.ipsum.Donec@eutellus.org', 'JPR23GPU1VL'),
(54, 10043, 'McKenzie', 'Chase', '8143 A Calle', '034644850716', 'Etiam.imperdiet.dictum@etultricesposuere.org', 'AJI93EXX2LS'),
(55, 10044, 'Kitra', 'Morgan', 'Apdo.:903-1010 Tincidunt Ctra.', '034862370464', 'metus@pedeblanditcongue.co.uk', 'LXI57TBE6DV'),
(56, 10045, 'Kelsie', 'Harrell', 'Apdo.:292-235 Dapibus Calle', '034644771877', 'tellus@Nullatinciduntneque.com', 'QIG53STS4IU'),
(57, 10046, 'Mari', 'Navarro', '951-3742 Risus. Avenida', '034115698705', 'eu.odio.tristique@egestasa.com', 'UFP59LBA3PG'),
(58, 10047, 'Bruno', 'Henson', '221 Ultrices. Avenida', '034479612485', 'natoque.penatibus.et@metus.ca', 'KHW72VEE4CF'),
(59, 10048, 'Camille', 'Hooper', '1985 Vel C/', '034488933204', 'nisi.Mauris.nulla@Phasellusin.edu', 'SJQ75EPF4KC'),
(60, 10049, 'Audrey', 'Holcomb', 'Apartado núm.: 579, 2585 Orci, Carretera', '034843458941', 'sit.amet@malesuadavel.edu', 'RCQ25MRL2HH'),
(61, 10050, 'Connor', 'Aguilar', 'Apartado núm.: 941, 3524 Non, Ctra.', '034608404410', 'est.Nunc@Duis.co.uk', 'ZEW29GSF1UN'),
(62, 10051, 'Mufutau', 'Hopper', '307-4670 Enim Calle', '034141942346', 'felis.Donec@euaugueporttitor.co.uk', 'TVA09JXB6NV'),
(63, 10052, 'Quynn', 'Dennis', '487-9785 Maecenas C.', '034460347859', 'libero.et@nondui.co.uk', 'GYI65KLB7QK'),
(64, 10053, 'Ryan', 'Diaz', 'Apdo.:799-9203 Eget Ctra.', '034493819291', 'Donec@Integervitaenibh.com', 'BAK23ZFY1OA'),
(65, 10054, 'Dolan', 'Ortiz', 'Apartado núm.: 937, 6929 Morbi Avda.', '034389204929', 'justo@et.net', 'STH81GOC6HD'),
(66, 10055, 'Emery', 'James', 'Apartado núm.: 553, 7298 Proin Av.', '034458186735', 'mauris@tellussem.com', 'XJR54VAH6VP'),
(67, 10056, 'Shellie', 'Kent', 'Apartado núm.: 139, 4653 Cursus Avenida', '034874614556', 'est.Mauris.eu@elitpellentesquea.com', 'GZN03UHI5GM'),
(68, 10057, 'Scarlett', 'Good', 'Apartado núm.: 912, 3555 Pede Carretera', '034939265811', 'dictum.eleifend.nunc@vitaedolor.com', 'KOJ58CHD3ZU'),
(69, 10058, 'Clayton', 'Wiley', '629-8587 Cras Avenida', '034847381208', 'et.magnis@convallis.co.uk', 'FUV51JVN8HA'),
(70, 10059, 'Caldwell', 'Dale', '7575 Dui Av.', '034746937550', 'Donec.nibh@atrisus.co.uk', 'KOX81POH5WY'),
(71, 10060, 'Elliott', 'Poole', '866-2189 Magna. Av.', '034550736714', 'ullamcorper@nuncrisusvarius.ca', 'EGC84PYN1VA'),
(72, 10061, 'Dillon', 'Stafford', 'Apdo.:569-2331 Duis Carretera', '034697271277', 'nascetur.ridiculus.mus@nuncIn.org', 'BOB41MXO7HO'),
(73, 10062, 'Amy', 'Bass', 'Apdo.:698-2782 Ligula. C/', '034918144135', 'amet@sitamet.ca', 'YXC69VPN6VW'),
(74, 10063, 'Ferris', 'Johnson', '362-4178 Sodales C/', '034252755141', 'accumsan.interdum.libero@massaInteger.co.uk', 'HAK29YRG8NE'),
(75, 10064, 'Travis', 'Tillman', 'Apartado núm.: 816, 2118 Vel, Calle', '034910196439', 'velit.Aliquam@nec.co.uk', 'DHB19CQL5KS'),
(76, 10065, 'Nissim', 'Nieves', 'Apartado núm.: 348, 277 Elementum, Avenida', '034190880275', 'velit.egestas@nectellusNunc.org', 'HMV55NXL3VO'),
(77, 10066, 'Zoe', 'Stephenson', 'Apartado núm.: 115, 5752 Nibh Carretera', '034872332451', 'aliquet.molestie@pharetrafeliseget.ca', 'GGT82XUL8VK'),
(78, 10067, 'Arthur', 'Torres', '8917 Vel Avda.', '034759513223', 'elit.dictum.eu@molestie.com', 'QFF12LIR6FD'),
(79, 10068, 'Indigo', 'Glenn', '5496 Et Avda.', '034490154699', 'at@porttitorvulputate.edu', 'IAN38YKT4UA'),
(80, 10069, 'Aquila', 'Gray', '679-3208 Semper ', '034987189309', 'a@necorci.co.uk', 'LQU20WPE4IT'),
(81, 10070, 'Brooke', 'Alford', '908-1760 Mi C.', '034203151778', 'dapibus.gravida.Aliquam@tortorIntegeraliquam.edu', 'JVY79WJR1ZX'),
(82, 10071, 'Matthew', 'Mcdonald', '829-1057 Placerat Av.', '034328724861', 'accumsan.neque.et@enimmi.co.uk', 'ZYC59CXW3GQ'),
(83, 10072, 'Blake', 'Elliott', 'Apdo.:192-6221 Nec, Calle', '034982834894', 'ac.orci.Ut@mattis.net', 'EVH37TLA5NU'),
(84, 10073, 'Eliana', 'Hayes', 'Apdo.:187-5219 Tristique ', '034120121839', 'felis.Nulla@arcu.co.uk', 'WQT44DKW9QY'),
(85, 10074, 'Hoyt', 'Rose', 'Apdo.:743-4669 Mauris, Av.', '034336142150', 'mauris.Suspendisse.aliquet@rutrum.org', 'PTD19GHC8UM'),
(86, 10075, 'Taylor', 'Macias', 'Apdo.:442-655 Etiam C/', '034559131753', 'auctor.velit.Aliquam@vitae.com', 'NEF76LHW1EH'),
(87, 10076, 'Lucius', 'Rose', 'Apartado núm.: 358, 1564 Curabitur ', '034899865892', 'interdum.enim.non@Suspendissesagittis.co.uk', 'MOC13HVV8GX'),
(88, 10077, 'Dominic', 'Kirkland', 'Apdo.:529-4646 Aliquet ', '034674792827', 'velit@vitaesemperegestas.com', 'WFA13YSZ6EY'),
(89, 10078, 'Skyler', 'Hawkins', 'Apdo.:205-3952 Vel C/', '034433354212', 'arcu@convallisantelectus.ca', 'ADK05KZG5GI'),
(90, 10079, 'Adrienne', 'Finch', '326-2618 Tempus, C/', '034140338754', 'mauris.erat.eget@elit.edu', 'QKC64SHY8OD'),
(91, 10080, 'Aurora', 'Terry', 'Apdo.:990-5211 Cursus. Avenida', '034360439761', 'euismod.enim@inconsequat.org', 'OLO14IWD3FS'),
(92, 10081, 'Leilani', 'Mitchell', 'Apartado núm.: 789, 7391 Iaculis Avenida', '034582687234', 'pulvinar.arcu.et@pede.com', 'PVI48USK1OJ'),
(93, 10082, 'Octavius', 'Hester', 'Apartado núm.: 881, 2986 Ante Avenida', '034901740845', 'montes.nascetur@insodaleselit.ca', 'GQR62JNP3MS'),
(94, 10083, 'Ria', 'Long', 'Apdo.:721-9680 Arcu Ctra.', '034314638383', 'id.erat@tristiquepharetraQuisque.edu', 'CUP96WPJ3PG'),
(95, 10084, 'Gray', 'Molina', 'Apartado núm.: 774, 5987 Eros C.', '034826868616', 'est.Mauris@tortorNunccommodo.net', 'BLK42QXT7BB'),
(96, 10085, 'Debra', 'Dunlap', '729-9190 Lacinia ', '034927627911', 'ipsum@massa.org', 'VTC69EDR5OX'),
(97, 10086, 'Amaya', 'Colon', 'Apdo.:305-3983 Suspendisse Avenida', '034304448756', 'lorem@dolor.ca', 'NIZ32ZQF6HD'),
(98, 10087, 'Otto', 'Burgess', 'Apdo.:548-5910 Amet, Avenida', '034471115582', 'id.ante.dictum@orcisem.edu', 'TMA89TTS4LC'),
(99, 10088, 'Kathleen', 'Hardin', 'Apartado núm.: 605, 8053 Metus Calle', '034315761137', 'metus.In.lorem@perconubia.org', 'YSB78EHS2FH'),
(100, 10089, 'Shad', 'Petersen', '741-8054 Vehicula Calle', '034519668511', 'ornare.tortor@eget.net', 'QDT56VOH0TX'),
(101, 10090, 'Adria', 'Glass', '979-1576 Cras Carretera', '034283587280', 'et.magna.Praesent@ante.com', 'WSC08WHH3BZ'),
(102, 10091, 'Regina', 'Wooten', '4163 Velit Avda.', '034310929567', 'viverra.Maecenas@montes.com', 'HFZ08QZJ8BR'),
(103, 10092, 'Kylynn', 'Adams', 'Apdo.:213-8506 Vestibulum Carretera', '034933469763', 'Nam.nulla.magna@tempor.net', 'BGL17QCT4XB'),
(104, 10093, 'Guy', 'Bolton', 'Apdo.:359-313 Semper Av.', '034672190995', 'amet@Maurisvestibulum.ca', 'ULV28LEK9AY'),
(105, 10094, 'Aurelia', 'Long', 'Apartado núm.: 295, 2304 Diam. Avenida', '034358295569', 'Curabitur.ut.odio@odiovelest.net', 'RAT81YPU1KC'),
(106, 10095, 'Zeph', 'Robertson', '929-5035 Ipsum. Avenida', '034353889296', 'orci.Ut@facilisisnonbibendum.net', 'AVB19WSQ7ZF'),
(107, 10096, 'Aurelia', 'Marks', '194-1628 At, Av.', '034506594731', 'auctor.ullamcorper@sed.com', 'GXY51EDY4NB'),
(108, 10097, 'Kimberley', 'Pena', '8324 Praesent Carretera', '034746719601', 'magna@pedemalesuada.co.uk', 'ZMT93BXN7KN'),
(109, 10098, 'Hamilton', 'William', '5719 Dapibus Avenida', '034310630993', 'blandit@turpis.ca', 'MDK50JID5IL'),
(110, 10099, 'Carter', 'Beck', '180-1100 Sit Calle', '034954228557', 'enim.Nunc.ut@facilisiSed.edu', 'CUC01ENY3DX'),
(111, 123456, 'abraham', 'fernandez sanchez', 'plz san esteban nº 6', '679269317', 'brajan@brajan.com', '1234'),
(113, 654321, 'nombrePrueba', 'apellidosPrueba', 'calle mas', '456987456', 'nombrePrueba@com.com', '1111'),
(114, 1234567, 'nompru', 'apellpru', 'calle prui', '456987412', 'nompru@apellpru.com', '1111'),
(120, 12345678, 'a', 'a', 'a', '1', 'a', '23'),
(126, 32131, 'werwrwrew', 'rewrwe', 'rewrwe', '321', 'rwerwe', 'rwere'),
(127, 45234, 'trwetet', 'trete', 'trete', '432', 'rwere', 'rwe');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `libro`
--
ALTER TABLE `libro`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `isbn` (`isbn`);

--
-- Indices de la tabla `prestamos_libros`
--
ALTER TABLE `prestamos_libros`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `carnet` (`carnet`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `libro`
--
ALTER TABLE `libro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT de la tabla `prestamos_libros`
--
ALTER TABLE `prestamos_libros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
